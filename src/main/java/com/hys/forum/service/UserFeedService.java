package com.hys.forum.service;

import com.hys.forum.pojo.request.PageRequest;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.pojo.Posts;
import com.hys.forum.pojo.UserFeed;

import java.util.List;


public interface UserFeedService {

    void batchSave(List<UserFeed> userFeeds);

    PageResult<Posts> pagePosts(PageRequest<Long> pageRequest);

    void deleteByPostsId(Long postsId);
}
