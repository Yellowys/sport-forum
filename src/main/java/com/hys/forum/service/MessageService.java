package com.hys.forum.service;

import com.hys.forum.common.enums.MessageTypeEn;
import com.hys.forum.pojo.Message;
import com.hys.forum.pojo.request.PageRequest;
import com.hys.forum.pojo.request.PageResult;

import java.util.List;

public interface MessageService {

    void send(Message message);

    void save(Message message);

    Message get(Long id);

    PageResult<Message> page(PageRequest<Message> pageRequest);

    void updateToRead(Message message);

    Long countUnRead(Long receiver);

    void deleteInTypesAndTitle(List<MessageTypeEn> typeEns, String title);
}
