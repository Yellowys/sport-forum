package com.hys.forum.service;


import com.hys.forum.pojo.Message;

public interface MailService {

    void sendHtml(Message mailMessage);

    void sendText(Message mailMessage);

}
