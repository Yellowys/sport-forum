package com.hys.forum.service;

import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.pojo.Article;
import com.hys.forum.pojo.value.PostsPageQueryValue;


public interface ArticleService {

    void save(Article article);

    Article get(Long id);

    void update(Article article);

    PageResult<Article> page(Integer pageNo, Integer pageSize, PostsPageQueryValue pageQueryValue);
}
