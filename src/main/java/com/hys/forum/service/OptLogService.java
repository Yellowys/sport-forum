package com.hys.forum.service;

import com.hys.forum.pojo.request.PageRequest;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.pojo.OptLog;

public interface OptLogService {

    void save(OptLog optLog);

    PageResult<OptLog> page(PageRequest<OptLog> pageRequest);
}
