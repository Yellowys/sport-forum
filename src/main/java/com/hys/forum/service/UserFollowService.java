package com.hys.forum.service;

import com.hys.forum.common.enums.FollowedTypeEn;

import java.util.List;


public interface UserFollowService {

    List<Long> getAllFollowerIds(Long follower, FollowedTypeEn type);
}
