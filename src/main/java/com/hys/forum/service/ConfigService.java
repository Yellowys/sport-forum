package com.hys.forum.service;

import com.hys.forum.pojo.request.PageRequest;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.pojo.Config;

import java.util.List;
import java.util.Set;

public interface ConfigService {

    /**
     * 保存
     * @param config
     */
    void save(Config config);

    /**
     * 查询
     * @param id
     * @return
     */
    Config get(Long id);

    /**
     * 更新
     * @param config
     */
    void update(Config config);

    /**
     * 根据类型查询可用
     * @param types
     * @return
     */
    List<Config> queryAvailable(Set<String> types);

    /**
     * 分页查询
     * @param configPageRequest
     * @return
     */
    PageResult<Config> page(PageRequest<Config> configPageRequest);
}
