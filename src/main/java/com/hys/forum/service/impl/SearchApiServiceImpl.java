package com.hys.forum.service.impl;

import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.vo.PostsVO;
import com.hys.forum.support.ResultModelUtil;
import com.hys.forum.common.validator.PageRequestModelValidator;
import org.springframework.stereotype.Service;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.service.api.SearchApiService;
import com.hys.forum.manager.SearchManager;

import javax.annotation.Resource;

/**
 * @author hys
 * @desc
 **/
@Service
public class SearchApiServiceImpl implements SearchApiService {

    @Resource
    private SearchManager searchManager;

    @Override
    public ResultModel<PageResponseModel<PostsVO>> pagePostsSearch(PageRequestModel<String> pageRequestModel) {
        PageRequestModelValidator.validator(pageRequestModel);

        return ResultModelUtil.success(searchManager.pagePostsSearch(pageRequestModel));
    }

}
