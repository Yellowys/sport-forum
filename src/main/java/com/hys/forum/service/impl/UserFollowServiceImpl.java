package com.hys.forum.service.impl;

import com.hys.forum.service.UserFollowService;
import org.springframework.stereotype.Service;
import com.hys.forum.common.enums.FollowedTypeEn;
import com.hys.forum.dao.UserFollowDAO;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hys
 * @desc
 **/
@Service
public class UserFollowServiceImpl implements UserFollowService {

    @Resource
    private UserFollowDAO userFollowDAO;

    @Override
    public List<Long> getAllFollowerIds(Long follower, FollowedTypeEn type) {
        return userFollowDAO.getAllFollowerIds(follower, type.getValue());
    }
}
