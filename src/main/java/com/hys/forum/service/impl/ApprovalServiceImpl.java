package com.hys.forum.service.impl;

import com.hys.forum.service.ApprovalService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.hys.forum.common.enums.FollowedTypeEn;
import com.hys.forum.pojo.Approval;
import com.hys.forum.dao.UserFollowDAO;
import com.hys.forum.pojo.dataobject.UserFollowDO;
import com.hys.forum.common.transfer.ApprovalTransfer;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hys
 * @desc
 **/
@Service
public class ApprovalServiceImpl implements ApprovalService {

    @Resource
    private UserFollowDAO userFollowDAO;

    @Override
    public void save(Approval approval) {
        UserFollowDO userFollowDO = ApprovalTransfer.toUserFollowDO(approval);
        userFollowDO.initBase();

        userFollowDAO.insert(userFollowDO);
    }

    @Override
    public void delete(Long approvalId) {
        userFollowDAO.delete(approvalId);
    }

    @Override
    public Approval get(Long postsId, Long userId) {
        List<UserFollowDO> userFollowDOS = userFollowDAO.query(UserFollowDO.builder()
                .follower(userId)
                .followed(postsId)
                .followedType(FollowedTypeEn.POSTS.getValue())
                .build());
        if (ObjectUtils.isEmpty(userFollowDOS)) {
            return null;
        }

        return ApprovalTransfer.toApproval(userFollowDOS.get(0));
    }
}
