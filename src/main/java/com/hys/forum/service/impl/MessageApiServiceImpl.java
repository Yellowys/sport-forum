package com.hys.forum.service.impl;

import com.alibaba.fastjson.JSON;
import com.hys.forum.support.ResultModelUtil;
import com.hys.forum.common.validator.MessageValidator;
import com.hys.forum.common.validator.PageRequestModelValidator;
import org.springframework.stereotype.Service;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.message.MessagePageRequest;
import com.hys.forum.pojo.response.message.MessagePageResponse;
import com.hys.forum.service.api.MessageApiService;
import com.hys.forum.manager.MessageManager;

import javax.annotation.Resource;

/**
 * @author hys
 * @desc
 **/
@Service
public class MessageApiServiceImpl implements MessageApiService {

    @Resource
    private MessageManager messageManager;

    @Override
    public ResultModel<PageResponseModel<MessagePageResponse>> page(PageRequestModel<MessagePageRequest> pageRequestModel) {
        PageRequestModelValidator.validator(pageRequestModel);
        pageRequestModel.setFilter(JSON.parseObject(JSON.toJSONString(pageRequestModel.getFilter()), MessagePageRequest.class));
        MessageValidator.page(pageRequestModel.getFilter());

        return ResultModelUtil.success(messageManager.page(pageRequestModel));
    }

    @Override
    public ResultModel markIsRead(Long messageId) {
        messageManager.markIsRead(messageId);

        return ResultModelUtil.success();
    }

    @Override
    public ResultModel<Long> countUnRead() {
        return ResultModelUtil.success(messageManager.countUnRead());
    }
}
