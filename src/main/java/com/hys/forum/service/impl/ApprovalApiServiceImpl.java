package com.hys.forum.service.impl;

import org.springframework.stereotype.Service;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.service.api.ApprovalApiService;
import com.hys.forum.manager.ApprovalManager;
import com.hys.forum.support.ResultModelUtil;

import javax.annotation.Resource;

/**
 * @author hys
 * @desc
 **/
@Service
public class ApprovalApiServiceImpl implements ApprovalApiService {

    @Resource
    private ApprovalManager approvalManager;

    @Override
    public ResultModel<Long> create(Long postsId) {
        return ResultModelUtil.success(approvalManager.create(postsId));
    }

    @Override
    public ResultModel<Long> delete(Long postsId) {
        return ResultModelUtil.success(approvalManager.delete(postsId));
    }

    @Override
    public ResultModel<Boolean> hasApproval(Long postsId) {
        return ResultModelUtil.success(approvalManager.hasApproval(postsId));
    }

}
