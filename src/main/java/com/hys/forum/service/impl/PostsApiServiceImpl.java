package com.hys.forum.service.impl;

import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.vo.PostsVO;
import com.hys.forum.support.ResultModelUtil;
import com.hys.forum.common.validator.ArticleValidator;
import org.springframework.stereotype.Service;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.AdminBooleanRequest;
import com.hys.forum.service.api.PostsApiService;
import com.hys.forum.manager.PostsManager;

import javax.annotation.Resource;

/**
 * @author hys
 * @desc
 **/
@Service
public class PostsApiServiceImpl implements PostsApiService {

    @Resource
    private PostsManager postsManager;

    @Override
    public ResultModel delete(Long id) {
        postsManager.delete(id);

        return ResultModelUtil.success();
    }

    @Override
    public ResultModel<PageResponseModel<PostsVO>> pagePostsFeed(PageRequestModel pageRequestModel) {
        return ResultModelUtil.success(postsManager.pagePostsFeed(pageRequestModel));
    }

    @Override
    public ResultModel auditState(AdminBooleanRequest booleanRequest) {
        ArticleValidator.validatorBooleanRequest(booleanRequest);

        postsManager.auditState(booleanRequest);

        return ResultModelUtil.success();
    }

}
