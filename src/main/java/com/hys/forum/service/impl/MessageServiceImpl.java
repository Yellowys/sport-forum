package com.hys.forum.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hys.forum.common.enums.MessageReadEn;
import com.hys.forum.common.enums.MessageTypeEn;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.dao.MessageDAO;
import com.hys.forum.pojo.dataobject.MessageDO;
import com.hys.forum.pojo.request.PageRequest;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.service.MailService;
import com.hys.forum.service.MessageService;
import com.hys.forum.common.transfer.MessageTransfer;
import org.springframework.stereotype.Service;
import com.hys.forum.common.enums.MessageChannelEn;
import com.hys.forum.common.enums.MessageContentTypeEn;
import com.hys.forum.pojo.Message;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hys
 * @desc
 **/
@Service
public class MessageServiceImpl implements MessageService {

    @Resource
    private MessageDAO messageDAO;

    @Resource
    private MessageService messageService;

    @Resource
    private MailService mailService;

    @Override
    public void send(Message message) {
        // 邮件
        if (MessageChannelEn.MAIL.equals(message.getChannel())) {
            if (MessageContentTypeEn.HTML.equals(message.getContentType())) {
                mailService.sendHtml(message);
            }
            if (MessageContentTypeEn.TEXT.equals(message.getContentType())) {
                mailService.sendText(message);
            }
        }

        // 站内信
        if (MessageChannelEn.STATION_LETTER.equals(message.getChannel())) {
            // do nothing
        }

        messageService.save(message);
    }


    @Override
    public void save(Message message) {
        String receiver = message.getReceiver().getId();
        String sender = message.getSender().getId();
        if (receiver.equals(sender)) {
            return;
        }

        MessageDO messageDO = MessageDO.builder()
                .channel(message.getChannel().getValue())
                .type(message.getType().getValue())
                .read(message.getRead().getValue())
                .sender(sender)
                .senderType(message.getSender().getType().getValue())
                .receiver(receiver)
                .receiverType(message.getReceiver().getType().getValue())
                .title(message.getTitle())
                .content(message.getContent())
                .contentType(message.getContentType().getValue())
                .build();
        messageDO.initBase();

        messageDAO.insert(messageDO);
    }

    @Override
    public Message get(Long id) {
        return MessageTransfer.toMessage(messageDAO.get(id));
    }

    @Override
    public void updateToRead(Message message) {
        MessageDO messageDO = MessageDO.builder()
                .read(message.getRead().getValue())
                .build();
        messageDO.setId(message.getId());
        messageDAO.updateToRead(messageDO);
    }

    @Override
    public Long countUnRead(Long receiver) {
        return messageDAO.countUnRead(receiver.toString(), MessageReadEn.NO.getValue());
    }

    @Override
    public void deleteInTypesAndTitle(List<MessageTypeEn> typeEns, String title) {
        List<String> types = SafesUtil.ofList(typeEns).stream().map(MessageTypeEn::getValue).collect(Collectors.toList());
        if (ObjectUtils.isEmpty(types) || ObjectUtils.isEmpty(title)) {
            return;
        }
        messageDAO.deleteInTypesAndTitle(types, title);
    }

    @Override
    public PageResult<Message> page(PageRequest<Message> pageRequest) {
        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());

        Message message = pageRequest.getFilter();
        MessageDO messageDO = MessageDO.builder().build();
        if (!ObjectUtils.isEmpty(message.getChannel())) {
            messageDO.setChannel(message.getChannel().getValue());
        }
        if (!ObjectUtils.isEmpty(message.getType())) {
            messageDO.setType(message.getType().getValue());
        }
        if (!ObjectUtils.isEmpty(message.getSender())) {
            messageDO.setSender(message.getSender().getId());
        }
        if (!ObjectUtils.isEmpty(message.getReceiver())) {
            messageDO.setReceiver(message.getReceiver().getId());
        }

        List<MessageDO> messageDOS = messageDAO.query(messageDO);
        PageInfo<MessageDO> pageInfo = new PageInfo<>(messageDOS);

        if (ObjectUtils.isEmpty(messageDOS)) {
            return PageResult.build(pageInfo.getTotal(), pageInfo.getSize(), new ArrayList<>());
        }

        return PageResult.build(pageInfo.getTotal(), pageInfo.getSize(), MessageTransfer.toMessages(messageDOS));
    }

}
