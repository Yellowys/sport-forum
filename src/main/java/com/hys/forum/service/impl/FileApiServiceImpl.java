package com.hys.forum.service.impl;

import com.hys.forum.support.ResultModelUtil;
import com.hys.forum.common.validator.FileValidator;
import org.springframework.stereotype.Service;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.file.FileUploadImgRequest;
import com.hys.forum.service.api.FileApiService;
import com.hys.forum.manager.FileManager;

import javax.annotation.Resource;

/**
 * @author hys
 * @desc
 **/
@Service
public class FileApiServiceImpl implements FileApiService {

    @Resource
    private FileManager fileManager;

    @Override
    public ResultModel<String> uploadImg(FileUploadImgRequest request) {
        FileValidator.uploadImg(request);

        return ResultModelUtil.success(fileManager.uploadImg(request));
    }
}
