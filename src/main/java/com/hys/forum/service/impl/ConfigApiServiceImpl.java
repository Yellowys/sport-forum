package com.hys.forum.service.impl;

import com.hys.forum.support.ResultModelUtil;
import com.hys.forum.common.validator.PageRequestModelValidator;
import org.springframework.stereotype.Service;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.AdminBooleanRequest;
import com.hys.forum.pojo.request.config.ConfigAddRequest;
import com.hys.forum.pojo.request.config.ConfigPageRequest;
import com.hys.forum.pojo.request.config.ConfigUpdateRequest;
import com.hys.forum.pojo.response.config.ConfigResponse;
import com.hys.forum.service.api.ConfigApiService;
import com.hys.forum.manager.ConfigManager;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * @author hys
 * @desc
 **/
@Service
public class ConfigApiServiceImpl implements ConfigApiService {

    @Resource
    private ConfigManager configManager;

    @Override
    public ResultModel add(ConfigAddRequest request) {

        configManager.add(request);

        return ResultModelUtil.success();
    }

    @Override
    public ResultModel update(ConfigUpdateRequest request) {

        configManager.update(request);

        return ResultModelUtil.success();
    }

    @Override
    public ResultModel state(AdminBooleanRequest request) {

        configManager.state(request);

        return ResultModelUtil.success();
    }

    @Override
    public ResultModel<PageResponseModel<ConfigResponse>> page(PageRequestModel<ConfigPageRequest> pageRequestModel) {
        PageRequestModelValidator.validator(pageRequestModel);

        return ResultModelUtil.success(configManager.page(pageRequestModel));
    }

    @Override
    public ResultModel<List<ConfigResponse>> queryAvailable(Set<String> types) {

        return ResultModelUtil.success(configManager.queryAvailable(types));
    }
}
