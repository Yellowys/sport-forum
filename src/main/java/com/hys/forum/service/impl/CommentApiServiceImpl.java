package com.hys.forum.service.impl;

import com.hys.forum.support.ResultModelUtil;
import com.hys.forum.common.validator.CommentValidator;
import com.hys.forum.common.validator.PageRequestModelValidator;
import org.springframework.stereotype.Service;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.comment.CommentCreateRequest;
import com.hys.forum.pojo.response.comment.CommentPageResponse;
import com.hys.forum.service.api.CommentApiService;
import com.hys.forum.manager.CommentManager;

import javax.annotation.Resource;

/**
 * @author hys
 * @desc
 **/
@Service
public class CommentApiServiceImpl implements CommentApiService {

    @Resource
    private CommentManager commentManager;

    @Override
    public ResultModel create(CommentCreateRequest request) {
        CommentValidator.create(request);

        commentManager.create(request);

        return ResultModelUtil.success();
    }

    @Override
    public ResultModel<PageResponseModel<CommentPageResponse>> page(PageRequestModel<Long> pageRequest) {
        PageRequestModelValidator.validator(pageRequest);

        return ResultModelUtil.success(commentManager.page(pageRequest));
    }
}
