package com.hys.forum.service.impl;

import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.support.ResultModelUtil;
import com.hys.forum.common.validator.FaqValidator;
import com.hys.forum.common.validator.PageRequestModelValidator;
import org.springframework.stereotype.Service;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.faq.*;
import com.hys.forum.pojo.response.faq.FaqHotsResponse;
import com.hys.forum.pojo.response.faq.FaqInfoResponse;
import com.hys.forum.pojo.response.faq.FaqUserPageResponse;
import com.hys.forum.service.api.FaqApiService;
import com.hys.forum.manager.FaqManager;
import com.hys.forum.common.enums.UserRoleEn;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hys
 * @desc
 **/
@Service
public class FaqApiServiceImpl implements FaqApiService {

    @Resource
    private FaqManager faqManager;

    @Override
    public ResultModel<Long> saveFaq(FaqSaveFaqRequest request) {
        FaqValidator.saveFaq(request);

        return ResultModelUtil.success(faqManager.saveFaq(request));
    }

//    @IsLogin(role = UserRoleEn.ADMIN)
    @Override
    public ResultModel<PageResponseModel<FaqUserPageResponse>> adminPage(PageRequestModel<FaqAdminPageRequest> pageRequestModel) {
        PageRequestModelValidator.validator(pageRequestModel);

        return ResultModelUtil.success(faqManager.adminPage(pageRequestModel));
    }

    @Override
    public ResultModel<PageResponseModel<FaqUserPageResponse>> userPage(PageRequestModel<FaqUserPageRequest> pageRequestModel) {
        PageRequestModelValidator.validator(pageRequestModel);

        return ResultModelUtil.success(faqManager.userPage(pageRequestModel));
    }

    @Override
    public ResultModel<PageResponseModel<FaqUserPageResponse>> authorPage(PageRequestModel<FaqAuthorPageRequest> pageRequestModel) {
        PageRequestModelValidator.validator(pageRequestModel);

        return ResultModelUtil.success(faqManager.authorPage(pageRequestModel));
    }

    @Override
    public ResultModel<FaqInfoResponse> info(Long id) {
        return ResultModelUtil.success(faqManager.info(id));
    }

    @Override
    public ResultModel<List<FaqHotsResponse>> hots(int size) {
        return ResultModelUtil.success(faqManager.hots(size));
    }

    @Override
    public ResultModel solution(FaqSolutionRequest request) {
        FaqValidator.solution(request);

        faqManager.solution(request);

        return ResultModelUtil.success();
    }
}
