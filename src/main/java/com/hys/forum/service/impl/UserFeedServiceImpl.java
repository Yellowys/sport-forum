package com.hys.forum.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hys.forum.service.UserFeedService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.hys.forum.pojo.request.PageRequest;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.pojo.Posts;
import com.hys.forum.pojo.UserFeed;
import com.hys.forum.dao.UserFeedDAO;
import com.hys.forum.pojo.dataobject.UserFeedDO;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hys
 * @desc
 **/
@Service
public class UserFeedServiceImpl extends AbstractPostsService implements UserFeedService {

    @Resource
    private UserFeedDAO userFeedDAO;

    @Override
    public void batchSave(List<UserFeed> userFeeds) {
        if (ObjectUtils.isEmpty(userFeeds)) {
            return;
        }

        userFeeds.forEach(userFeed -> {
            try {
                UserFeedDO userFeedDO = UserFeedDO.builder()
                        .userId(userFeed.getUserId())
                        .postsId(userFeed.getPostsId())
                        .build();
                userFeedDO.initBase();

                userFeedDAO.insert(userFeedDO);
            } catch (Exception e) {
                // 唯一健冲突忽略
            }
        });
    }

    @Override
    public PageResult<Posts> pagePosts(PageRequest<Long> pageRequest) {
        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());

        List<UserFeedDO> userFeedDOS = userFeedDAO.query(pageRequest.getFilter());
        PageInfo<UserFeedDO> pageInfo = new PageInfo<>(userFeedDOS);

        if (ObjectUtils.isEmpty(userFeedDOS)) {
            return PageResult.build(pageInfo.getTotal(), pageInfo.getSize(), new ArrayList<>());
        }

        List<Long> postsIds = new ArrayList<>();
        userFeedDOS.forEach(userFeedDO -> postsIds.add(userFeedDO.getPostsId()));

        return basePagePosts(postsIds, pageInfo, null);
    }

    @Override
    public void deleteByPostsId(Long postsId) {
        userFeedDAO.deleteByPostsId(postsId);
    }
}
