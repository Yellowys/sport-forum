package com.hys.forum.service;

import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.pojo.Faq;
import com.hys.forum.pojo.value.PostsPageQueryValue;

import java.util.List;


public interface FaqService {

    void save(Faq faq);

    void update(Faq faq);

    void updateEntity(Faq faq);

    Faq get(Long id);

    PageResult<Faq> page(Integer pageNo, Integer pageSize, PostsPageQueryValue pageQueryValue);

    List<Faq> hots(int size);

}
