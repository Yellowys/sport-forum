package com.hys.forum.service;


public interface FileService {

    String uploadImg(byte[] base64, String fileName);
}
