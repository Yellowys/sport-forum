package com.hys.forum.service;

import com.hys.forum.pojo.Approval;


public interface ApprovalService {

    void save(Approval approval);

    void delete(Long approvalId);

    Approval get(Long postsId, Long userId);

}
