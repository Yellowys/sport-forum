package com.hys.forum.service;

import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.pojo.Comment;

import java.util.List;
import java.util.Set;


public interface CommentService {

    void save(Comment comment);

    Comment get(Long id);

    List<Comment> queryByPostsId(Long postsId);

    List<Comment> queryInReplyIds(Set<Long> replyIds);

    PageResult<Comment> page(Integer pageNo, Integer pageSize, Long postsId);

    void deleteByPostsId(Long postsId);

    List<Comment> queryInIds(Set<Long> ids);
}
