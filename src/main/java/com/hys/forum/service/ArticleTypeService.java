package com.hys.forum.service;

import com.hys.forum.common.enums.ArticleTypeScopeEn;
import com.hys.forum.common.enums.AuditStateEn;
import com.hys.forum.pojo.request.PageRequest;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.pojo.ArticleType;

import java.util.List;


public interface ArticleTypeService {

    void save(ArticleType articleType);

    List<ArticleType> query(ArticleType articleType);

    List<ArticleType> queryByState(AuditStateEn auditState);

    List<ArticleType> queryByScopesAndState(List<ArticleTypeScopeEn> scopes, AuditStateEn auditState);

    void update(ArticleType articleType);

    ArticleType get(Long id);

    ArticleType getByNameAndState(String typeName, AuditStateEn pass);

    void increaseRefCount(Long id);

    void decreaseRefCount(Long id);

    PageResult<ArticleType> page(PageRequest<ArticleType> articleTypePageRequest);
}
