package com.hys.forum.service.api;

import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.AdminBooleanRequest;
import com.hys.forum.pojo.request.config.ConfigAddRequest;
import com.hys.forum.pojo.request.config.ConfigPageRequest;
import com.hys.forum.pojo.request.config.ConfigUpdateRequest;
import com.hys.forum.pojo.response.config.ConfigResponse;

import java.util.List;
import java.util.Set;


public interface ConfigApiService {

    /**
     * 添加
     * @param request
     * @return
     */
    ResultModel add(ConfigAddRequest request);

    /**
     * 更新
     * @param request
     * @return
     */
    ResultModel update(ConfigUpdateRequest request);

    /**
     * 更新状态
     * @param request
     * @return
     */
    ResultModel state(AdminBooleanRequest request);

    /**
     * 分页查询
     * @param pageRequestModel
     * @return
     */
    ResultModel<PageResponseModel<ConfigResponse>> page(PageRequestModel<ConfigPageRequest> pageRequestModel);

    /**
     * 类别查询可用
     * @param types
     * @return
     */
    ResultModel<List<ConfigResponse>> queryAvailable(Set<String> types);
}
