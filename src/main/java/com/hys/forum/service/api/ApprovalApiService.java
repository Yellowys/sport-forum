package com.hys.forum.service.api;

import com.hys.forum.pojo.model.ResultModel;


public interface ApprovalApiService {

    ResultModel<Long> create(Long postsId);

    ResultModel<Long> delete(Long postsId);

    ResultModel<Boolean> hasApproval(Long postsId);

}
