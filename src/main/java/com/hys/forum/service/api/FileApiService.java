package com.hys.forum.service.api;

import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.file.FileUploadImgRequest;

public interface FileApiService {

    ResultModel<String> uploadImg(FileUploadImgRequest request);

}
