package com.hys.forum.service.api;

import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.github.GithubOauthLoginRequest;


public interface GithubApiService {

    ResultModel<String> oauthLogin(GithubOauthLoginRequest request);

}
