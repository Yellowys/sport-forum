package com.hys.forum.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.hys.forum.common.enums.AuditStateEn;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.dao.PostsDAO;
import com.hys.forum.pojo.dataobject.PostsDO;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hys
 * @desc 帖子自动审核通过
 **/
@Service
public class PostsAuditTask {

    @Resource
    private PostsDAO postsDAO;

    @Scheduled(cron = "0/2 * * * * ? ")
    public void task() {
        List<PostsDO> postsDOS = postsDAO.query(PostsDO.builder()
                .auditState(AuditStateEn.WAIT.getValue())
                .build());
        SafesUtil.ofList(postsDOS).forEach(postsDO -> {
            postsDO.setAuditState(AuditStateEn.PASS.getValue());
            postsDAO.update(postsDO);
        });
    }

}
