package com.hys.forum.common.support;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.ObjectUtils;


public class AvatarUtil {

    // 默认头像
    private static final String GRAVATAR_URL = "https://sdn.geekzu.org/avatar/%s?d=retro";

    public static String get(String avatar, String email) {
        return ObjectUtils.isEmpty(avatar) ? String.format(GRAVATAR_URL, DigestUtils.md5Hex(ObjectUtils.isEmpty(email) ? "" : email)) : avatar;
    }

}
