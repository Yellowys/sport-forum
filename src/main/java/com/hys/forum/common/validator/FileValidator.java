package com.hys.forum.common.validator;

import com.hys.forum.pojo.request.file.FileUploadImgRequest;
import com.hys.forum.common.support.CheckUtil;


public class FileValidator {

    public static void uploadImg(FileUploadImgRequest request) {
        CheckUtil.checkParamToast(request, "request");
        CheckUtil.checkParamToast(request.getBase64(), "base64");
        CheckUtil.checkParamToast(request.getFileName(), "fileName");
    }
}
