package com.hys.forum.common.validator;

import com.hys.forum.pojo.request.comment.CommentCreateRequest;
import com.hys.forum.common.support.CheckUtil;


public class CommentValidator {

    public static void create(CommentCreateRequest request) {
        CheckUtil.checkParamToast(request, "request");
        CheckUtil.checkParamToast(request.getPostsId(), "postsId");
        CheckUtil.checkParamToast(request.getContent(), "content");
    }
}
