package com.hys.forum.common.validator;

import com.hys.forum.pojo.request.tag.TagCreateRequest;
import com.hys.forum.common.support.CheckUtil;


public class TagValidator {

    public static void create(TagCreateRequest request) {
        CheckUtil.checkParamToast(request, "request");
        CheckUtil.checkParamToast(request.getName(), "name");
        CheckUtil.checkParamToast(request.getGroupName(), "groupName");
        CheckUtil.checkParamToast(request.getDescription(), "description");
    }

}
