package com.hys.forum.common.validator;

import com.hys.forum.pojo.request.faq.FaqSaveFaqRequest;
import com.hys.forum.pojo.request.faq.FaqSolutionRequest;
import com.hys.forum.common.support.CheckUtil;


public class FaqValidator {

    public static void saveFaq(FaqSaveFaqRequest request) {
        CheckUtil.checkParamToast(request, "request");
        CheckUtil.checkParamToast(request.getTitle(), "title");
        CheckUtil.checkParamToast(request.getContentType(), "contentType");
        CheckUtil.checkParamToast(request.getMarkdownContent(), "markdownContent");
        CheckUtil.checkParamToast(request.getHtmlContent(), "htmlContent");
        CheckUtil.checkParamToast(request.getTagIds(), "tagIds");
    }

    public static void solution(FaqSolutionRequest request) {
        CheckUtil.checkParamToast(request, "request");
        CheckUtil.checkParamToast(request.getFaqId(), "faqId");
        CheckUtil.checkParamToast(request.getCommentId(), "commentId");
    }
}
