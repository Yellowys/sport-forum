package com.hys.forum.common.validator;

import com.hys.forum.pojo.request.message.MessagePageRequest;
import com.hys.forum.common.enums.MessageTypeEn;
import com.hys.forum.common.support.CheckUtil;


public class MessageValidator {

    public static void page(MessagePageRequest request) {
        CheckUtil.checkParamToast(request, "request");
        CheckUtil.checkParamToast(request.getTypeDesc(), "typeDesc");
        CheckUtil.checkParamToast(MessageTypeEn.getEntityByDesc(request.getTypeDesc()), "不存在的消息类型");
    }
}
