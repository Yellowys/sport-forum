package com.hys.forum.common.transfer;

import com.hys.forum.pojo.BasePosts;
import com.hys.forum.pojo.User;
import com.hys.forum.pojo.response.message.MessagePageResponse;
import org.springframework.util.ObjectUtils;
import com.hys.forum.common.enums.*;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.pojo.Message;
import com.hys.forum.pojo.value.IdValue;
import com.hys.forum.pojo.dataobject.MessageDO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hys
 * @desc
 **/
public class MessageTransfer {

    public static List<MessagePageResponse> toMessagePageResponses(List<Message> messages, List<User> users, List<BasePosts> postsList, User loginUser) {
        List<MessagePageResponse> res = new ArrayList<>();

        SafesUtil.ofList(messages).forEach(message -> {
            MessagePageResponse messagePageResponse = MessagePageResponse.builder()
                    .id(message.getId())
                    .read(message.getRead().getValue())
                    .sender(message.getSender().getId())
                    .typeDesc(message.getType().getDesc())
                    .createAt(message.getCreateAt())
                    .build();

            SafesUtil.ofList(users).forEach(user -> {
                if (user.getId().toString().equals(message.getSender().getId())) {
                    messagePageResponse.setSenderName(user.getNickname());
                    messagePageResponse.setSenderAvatar(user.getAvatar());
                }
            });

            if (MessageTypeEn.APPROVAL_ARTICLE.equals(message.getType())
                    || MessageTypeEn.APPROVAL_FAQ.equals(message.getType())
                    || MessageTypeEn.COMMENT_ARTICLE.equals(message.getType())
                    || MessageTypeEn.COMMENT_FAQ.equals(message.getType())) {
                SafesUtil.ofList(postsList).forEach(posts -> {
                    if (posts.getId().equals(Long.valueOf(message.getTitle()))) {
                        messagePageResponse.setTitle(posts.getTitle());
                        messagePageResponse.setInfoId(posts.getId().toString());
                    }
                });
            }
            if (MessageTypeEn.FOLLOW_USER.equals(message.getType())) {
                messagePageResponse.setTitle("关注了你");
                messagePageResponse.setInfoId(loginUser.getId().toString());
            }

            res.add(messagePageResponse);
        });

        return res;
    }

    public static Message toMessage(MessageDO messageDO) {
        if (ObjectUtils.isEmpty(messageDO)) {
            return null;
        }
        Message message = Message.builder()
                .title(messageDO.getTitle())
                .read(MessageReadEn.getEntity(messageDO.getRead()))
                .receiver(IdValue.builder()
                        .id(messageDO.getReceiver())
                        .type(IdValueTypeEn.getEntity(messageDO.getReceiverType()))
                        .build())
                .sender(IdValue.builder()
                        .id(messageDO.getSender())
                        .type(IdValueTypeEn.getEntity(messageDO.getSenderType()))
                        .build())
                .type(MessageTypeEn.getEntity(messageDO.getType()))
                .content(messageDO.getContent())
                .channel(MessageChannelEn.getEntity(messageDO.getChannel()))
                .contentType(MessageContentTypeEn.getEntity(messageDO.getContentType()))
                .build();
        message.setId(messageDO.getId());
        message.setCreateAt(messageDO.getCreateAt());
        message.setUpdateAt(messageDO.getUpdateAt());

        return message;
    }

    public static List<Message> toMessages(List<MessageDO> messageDOS) {
        List<Message> res = new ArrayList<>();

        SafesUtil.ofList(messageDOS).forEach(messageDO -> {
            res.add(toMessage(messageDO));
        });

        return res;
    }
}
