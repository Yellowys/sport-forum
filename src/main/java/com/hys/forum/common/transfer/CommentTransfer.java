package com.hys.forum.common.transfer;

import com.hys.forum.pojo.User;
import com.hys.forum.pojo.request.comment.CommentCreateRequest;
import com.hys.forum.pojo.response.comment.CommentPageResponse;
import org.springframework.util.ObjectUtils;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.pojo.Comment;
import com.hys.forum.pojo.dataobject.CommentDO;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hys
 * @desc
 **/
public class CommentTransfer {

    public static CommentDO toCommentDO(Comment comment) {
        CommentDO commentDO = CommentDO.builder()
                .content(comment.getContent())
                .postsId(comment.getPostsId())
                .replyId(comment.getReplyId())
                .replyReplyId(comment.getReplyReplyId())
                .userId(comment.getUserId())
                .build();
        commentDO.setId(comment.getId());
        commentDO.initBase();

        return commentDO;
    }

    public static List<Comment> toComments(List<CommentDO> commentDOs) {
        return SafesUtil.ofList(commentDOs).stream()
                .map(CommentTransfer::toComment)
                .collect(Collectors.toList());
    }

    public static Comment toComment(CommentDO commentDO) {
        if (ObjectUtils.isEmpty(commentDO)) {
            return null;
        }

        Comment comment = Comment.builder()
                .content(commentDO.getContent())
                .postsId(commentDO.getPostsId())
                .replyId(commentDO.getReplyId())
                .replyReplyId(commentDO.getReplyReplyId())
                .userId(commentDO.getUserId())
                .build();
        comment.setId(commentDO.getId());
        comment.setCreateAt(commentDO.getCreateAt());
        comment.setUpdateAt(commentDO.getUpdateAt());

        return comment;
    }

    public static Comment toComment(CommentCreateRequest request, User user) {
        return Comment.builder()
                .content(request.getContent())
                .postsId(request.getPostsId())
                .replyId(request.getReplyId())
                .userId(user.getId())
                .build();
    }

    public static List<CommentPageResponse> toCommentPageResponses(List<Comment> comments, List<User> users, Boolean isReply) {
        return SafesUtil.ofList(comments).stream().map(comment -> {
            CommentPageResponse.Commentator.CommentatorBuilder commentatorBuilder = CommentPageResponse.Commentator.builder();
            buildCommentator(commentatorBuilder, users, comment.getUserId());

            CommentPageResponse.Commentator.CommentatorBuilder respondentBuilder = CommentPageResponse.Commentator.builder();
            if (isReply && !ObjectUtils.isEmpty(comment.getReplyReplyId())) {
                SafesUtil.ofList(comments).forEach(articleCommentDO1 -> {
                    if (comment.getReplyReplyId().equals(articleCommentDO1.getId())) {
                        buildCommentator(respondentBuilder, users, articleCommentDO1.getUserId());
                    }
                });
            }
            return CommentPageResponse.builder()
                    .id(comment.getId())
                    .createAt(comment.getCreateAt())
                    .content(comment.getContent())
                    .commentator(commentatorBuilder.build())
                    .respondent(respondentBuilder.build())
                    .replyId(isReply ? comment.getReplyId() : null)
                    .build();
        }).collect(Collectors.toList());
    }

    private static void buildCommentator(CommentPageResponse.Commentator.CommentatorBuilder commentatorBuilder, List<User> users, Long uid) {
        SafesUtil.ofList(users).forEach(user -> {
            if (uid.equals(user.getId())) {
                commentatorBuilder.id(user.getId())
                        .nickname(user.getNickname())
                        .avatar(user.getAvatar());
            }
        });
    }
}
