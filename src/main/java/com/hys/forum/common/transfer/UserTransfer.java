package com.hys.forum.common.transfer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hys.forum.common.support.DateUtil;
import com.hys.forum.common.support.StringUtil;
import com.hys.forum.pojo.OptLog;
import com.hys.forum.pojo.request.user.UserAdminPageRequest;
import com.hys.forum.pojo.request.user.UserRegisterRequest;
import com.hys.forum.pojo.request.user.UserUpdateInfoRequest;
import com.hys.forum.pojo.response.user.UserInfoResponse;
import com.hys.forum.pojo.response.user.UserOptLogPageResponse;
import com.hys.forum.pojo.response.user.UserPageResponse;
import org.springframework.util.ObjectUtils;
import com.hys.forum.common.enums.*;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.pojo.Follow;
import com.hys.forum.pojo.User;
import com.hys.forum.pojo.dataobject.UserDO;
import com.hys.forum.pojo.dataobject.UserFollowDO;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author hys
 * @desc
 **/
public class UserTransfer {

    private static final String EXT_KET_GITHUB_USER = "githubUser";

    public static List<UserOptLogPageResponse> toUserOptLogPageResponses(List<OptLog> optLogs, List<User> userList) {
        return SafesUtil.ofList(optLogs).stream().map(optLog -> {
            UserOptLogPageResponse response = UserOptLogPageResponse.builder()
                    .content(optLog.getContent())
                    .createAt(DateUtil.toyyyyMMddHHmmss(optLog.getCreateAt()))
                    .type(optLog.getType().getDesc())
                    .build();
            SafesUtil.ofList(userList).forEach(user -> {
                if (user.getId().equals(optLog.getOperatorId())) {
                    response.setAvatar(user.getAvatar());
                    response.setEmail(user.getEmail());
                    response.setNickname(user.getNickname());
                    response.setUserId(user.getId());
                }
            });

            return response;
        }).collect(Collectors.toList());
    }

    public static UserInfoResponse toUserInfoResponse(User user) {
        return UserInfoResponse.builder()
                .avatar(user.getAvatar())
                .email(user.getEmail())
                .id(user.getId())
                .nickname(user.getNickname())
                .role(user.getRole().getValue())
                .sex(user.getSex().getDesc())
                .signature(user.getSignature())
                .createAt(user.getCreateAt())
                .lastLoginTime(user.getLastLoginTime())
                .build();
    }

    public static List<UserPageResponse> toUserPageResponses(List<User> users) {
        if (ObjectUtils.isEmpty(users)) {
            return new ArrayList<>();
        }

        return users.stream().map(UserTransfer::buildUserPageResponse).collect(Collectors.toList());
    }

    public static User toGithubUser(JSONObject githubUser, String email, String nickname, String signature, String avatar) {
        return User.builder()
                .email(email)
                .state(UserStateEn.UN_ACTIVATION)
                .source(UserSourceEn.GITHUB)
                .nickname(nickname)
                .password(StringUtil.md5UserPassword(email))
                .signature(signature)
                .role(UserRoleEn.USER)
                .avatar(avatar)
                .sex(UserSexEn.UNKNOWN)
                .githubUser(githubUser)
                .lastLoginTime(new Date())
                .build();
    }

    public static User toUser(UserRegisterRequest request) {
        return User.builder()
                .email(request.getEmail())
                .state(UserStateEn.UN_ACTIVATION)
                .source(UserSourceEn.REGISTER)
                .nickname(request.getNickname())
                .password(StringUtil.md5UserPassword(request.getPassword()))
                .signature("")
                .role(UserRoleEn.USER)
                .avatar("")
                .sex(UserSexEn.UNKNOWN)
                .lastLoginTime(new Date())
                .build();
    }

    public static User toUser(User user, UserUpdateInfoRequest request) {
        user.setNickname(request.getNickname());
        user.setSignature(request.getSignature());
        user.setEmail(request.getEmail());

        return user;
    }

    public static User toUser(UserAdminPageRequest request) {
        User user = User.builder()
                .nickname(request.getNickname())
                .email(request.getEmail())
                .build();
        user.setRole(UserRoleEn.getEntity(request.getRole()));
        user.setState(UserStateEn.getEntity(request.getState()));

        return user;
    }

    private static UserPageResponse buildUserPageResponse(User user) {
        return UserPageResponse.builder()
                .avatar(user.getAvatar())
                .email(user.getEmail())
                .id(user.getId())
                .nickname(user.getNickname())
                .role(user.getRole().getDesc())
                .sex(user.getSex().getDesc())
                .signature(user.getSignature())
                .state(user.getState().getDesc())
                .createAt(DateUtil.toyyyyMMddHHmmss(user.getCreateAt()))
                .updateAt(DateUtil.toyyyyMMddHHmmss(user.getUpdateAt()))
                .build();
    }

    public static Follow toFollow(UserFollowDO userFollowDO) {
        Follow follow = Follow.builder()
                .followed(userFollowDO.getFollowed())
                .followedType(FollowedTypeEn.getEntity(userFollowDO.getFollowedType()))
                .follower(userFollowDO.getFollower())
                .build();
        follow.setCreateAt(userFollowDO.getCreateAt());
        follow.setId(userFollowDO.getId());
        follow.setUpdateAt(userFollowDO.getUpdateAt());

        return follow;
    }

    public static UserDO toUserDO(User user) {
        if (ObjectUtils.isEmpty(user)) {
            return null;
        }

        UserDO userDO = UserDO.builder()
                .ext("{}")
                .avatar(user.getAvatar())
                .email(user.getEmail())
                .nickname(user.getNickname())
                .password(user.getPassword())
                .signature(user.getSignature())
                .lastLoginTime(user.getLastLoginTime())
                .build();

        if (!ObjectUtils.isEmpty(user.getSource())) {
            userDO.setSource(user.getSource().getValue());
        }
        if (!ObjectUtils.isEmpty(user.getGithubUser())) {
            Map<String, Object> ext = new HashMap<>();
            ext.put(EXT_KET_GITHUB_USER, user.getGithubUser());
            userDO.setExt(JSON.toJSONString(ext));
        }
        if (!ObjectUtils.isEmpty(user.getRole())) {
            userDO.setRole(user.getRole().getValue());
        }
        if (!ObjectUtils.isEmpty(user.getState())) {
            userDO.setState(user.getState().getValue());
        }
        if (!ObjectUtils.isEmpty(user.getSex())) {
            userDO.setSex(user.getSex().getValue());
        }
        userDO.setId(user.getId());
        userDO.setCreateAt(user.getCreateAt());
        userDO.setUpdateAt(user.getUpdateAt());

        return userDO;
    }

    public static User toUser(UserDO userDO) {
        if (ObjectUtils.isEmpty(userDO)) {
            return null;
        }

        User user = User.builder()
                .avatar(userDO.getAvatar())
                .source(UserSourceEn.getEntity(userDO.getSource()))
                .state(UserStateEn.getEntity(userDO.getState()))
                .email(userDO.getEmail())
                .nickname(userDO.getNickname())
                .password(userDO.getPassword())
                .role(UserRoleEn.getEntity(userDO.getRole()))
                .sex(UserSexEn.getEntity(userDO.getSex()))
                .signature(userDO.getSignature())
                .lastLoginTime(userDO.getLastLoginTime())
                .build();

        if (!ObjectUtils.isEmpty(userDO.getExt())) {
            JSONObject ext = JSON.parseObject(userDO.getExt());
            user.setGithubUser(ext.getJSONObject(EXT_KET_GITHUB_USER));
        }

        user.setId(userDO.getId());
        user.setCreateAt(userDO.getCreateAt());
        user.setUpdateAt(userDO.getUpdateAt());

        return user;
    }

    public static List<User> toUsers(List<UserDO> userDOS) {
        List<User> users = new ArrayList<>();

        SafesUtil.ofList(userDOS).forEach(userDO -> users.add(toUser(userDO)));

        return users;
    }
}
