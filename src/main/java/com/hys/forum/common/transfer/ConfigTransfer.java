package com.hys.forum.common.transfer;

import com.hys.forum.pojo.request.config.ConfigAddRequest;
import com.hys.forum.pojo.request.config.ConfigPageRequest;
import com.hys.forum.pojo.request.config.ConfigUpdateRequest;
import com.hys.forum.pojo.response.config.ConfigResponse;
import org.springframework.util.ObjectUtils;
import com.hys.forum.common.enums.AuditStateEn;
import com.hys.forum.common.enums.ConfigTypeEn;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.pojo.Config;
import com.hys.forum.pojo.dataobject.ConfigDO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hys
 * @desc
 **/
public class ConfigTransfer {
    public static Config toConfig(ConfigPageRequest request) {
        return Config.builder()
                .state(AuditStateEn.getEntity(request.getState()))
                .name(request.getName())
                .type(ConfigTypeEn.getEntity(request.getType()))
                .build();
    }

    public static List<ConfigResponse> toConfigResponses(List<Config> configList) {
        List<ConfigResponse> responses = new ArrayList<>();

        SafesUtil.ofList(configList).forEach(config -> responses.add(ConfigResponse.builder()
                .id(config.getId())
                .createAt(config.getCreateAt())
                .updateAt(config.getUpdateAt())
                .content(config.getContent())
                .creator(config.getCreator())
                .endAt(config.getEndAt())
                .name(config.getName())
                .startAt(config.getStartAt())
                .state(config.getState().getDesc())
                .type(config.getType().getDesc())
                .build()));

        return responses;
    }

    public static Config toConfig(ConfigAddRequest request) {
        return Config.builder()
                .content(request.getContent())
                .endAt(request.getEndAt())
                .name(request.getName())
                .startAt(request.getStartAt())
                .state(AuditStateEn.WAIT)
                .type(ConfigTypeEn.getEntity(request.getType()))
                .build();
    }

    public static void update(Config config, ConfigUpdateRequest request) {
        config.setContent(request.getContent());
        config.setEndAt(request.getEndAt());
        config.setName(request.getName());
        config.setStartAt(request.getStartAt());
        config.setType(ConfigTypeEn.getEntity(request.getType()));
        config.setState(AuditStateEn.WAIT);
    }

    public static List<Config> toConfigs(List<ConfigDO> configDOList) {
        List<Config> res = new ArrayList<>();

        SafesUtil.ofList(configDOList).forEach(configDO -> res.add(toConfig(configDO)));

        return res;
    }

    public static Config toConfig(ConfigDO configDO) {
        if (configDO == null) {
            return null;
        }
        Config config = new Config();
        config.setState(AuditStateEn.getEntity(configDO.getState()));
        config.setType(ConfigTypeEn.getEntity(configDO.getType()));
        config.setName(configDO.getName());
        config.setContent(configDO.getContent());
        config.setStartAt(configDO.getStartAt());
        config.setEndAt(configDO.getEndAt());
        config.setCreator(configDO.getCreator());
        config.setId(configDO.getId());
        config.setCreateAt(configDO.getCreateAt());
        config.setUpdateAt(configDO.getUpdateAt());
        return config;
    }

    public static ConfigDO toConfigDO(Config config) {
        if (config == null) {
            return null;
        }
        ConfigDO configDO = new ConfigDO();
        if (!ObjectUtils.isEmpty(config.getState())) {
            configDO.setState(config.getState().getValue());
        }
        if (!ObjectUtils.isEmpty(config.getType())) {
            configDO.setType(config.getType().getValue());
        }
        configDO.setName(config.getName());
        configDO.setContent(config.getContent());
        configDO.setStartAt(config.getStartAt());
        configDO.setEndAt(config.getEndAt());
        configDO.setCreator(config.getCreator());
        configDO.setId(config.getId());
        configDO.setCreateAt(config.getCreateAt());
        configDO.setUpdateAt(config.getUpdateAt());
        return configDO;
    }
}
