package com.hys.forum.common.transfer;

import com.hys.forum.common.support.DateUtil;
import com.hys.forum.pojo.request.tag.TagCreateRequest;
import com.hys.forum.pojo.response.tag.TagPageResponse;
import com.hys.forum.pojo.response.tag.TagQueryResponse;
import com.hys.forum.support.LoginUserContext;
import org.springframework.util.ObjectUtils;
import com.hys.forum.common.enums.AuditStateEn;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.pojo.Faq;
import com.hys.forum.pojo.Tag;
import com.hys.forum.pojo.dataobject.PostsDO;
import com.hys.forum.pojo.dataobject.TagDO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hys
 * @desc
 **/
public class TagTransfer {
    public static Tag toTag(TagCreateRequest request) {
        return Tag.builder()
                .groupName(request.getGroupName())
                .auditState(AuditStateEn.WAIT)
                .creatorId(LoginUserContext.getUser().getId())
                .description(request.getDescription())
                .name(request.getName())
                .refCount(0L)
                .build();
    }

    public static List<TagQueryResponse> toTagQueryAllResponses(List<Tag> tags) {
        List<TagQueryResponse> responses = new ArrayList<>();

        SafesUtil.ofList(tags).forEach(tag -> responses.add(toTagQueryAllResponse(tag)));
        return responses;
    }

    public static TagQueryResponse toTagQueryAllResponse(Tag tag) {
        return TagQueryResponse.builder()
                .description(tag.getDescription())
                .groupName(tag.getGroupName())
                .id(tag.getId())
                .name(tag.getName())
                .refCount(tag.getRefCount())
                .build();
    }

    public static List<TagPageResponse> toTagPageResponses(List<Tag> tags) {
        List<TagPageResponse> responses = new ArrayList<>();

        SafesUtil.ofList(tags).forEach(tag -> responses.add(TagPageResponse.builder()
                .auditState(tag.getAuditState().getDesc())
                .createAt(DateUtil.toyyyyMMddHHmmss(tag.getCreateAt()))
                .creatorId(tag.getCreatorId())
                .description(tag.getDescription())
                .groupName(tag.getGroupName())
                .id(tag.getId())
                .name(tag.getName())
                .refCount(tag.getRefCount())
                .updateAt(DateUtil.toyyyyMMddHHmmss(tag.getUpdateAt()))
                .build()));

        return responses;
    }

    public static List<Faq> toFaqs(List<PostsDO> postsDOS) {
        List<Faq> faqs = new ArrayList<>();

        SafesUtil.ofList(postsDOS).forEach(postsDO -> {
            Faq faq = new Faq();
            faq.setId(postsDO.getId());
            faq.setTitle(postsDO.getTitle());
            faq.setCreateAt(postsDO.getCreateAt());

            faqs.add(faq);
        });

        return faqs;
    }

    public static TagDO toTagDO(Tag tag) {
        TagDO tagDO = TagDO.builder()
                .auditState(ObjectUtils.isEmpty(tag.getAuditState()) ? null : tag.getAuditState().getValue())
                .creatorId(tag.getCreatorId())
                .groupName(tag.getGroupName())
                .description(tag.getDescription())
                .name(tag.getName())
                .refCount(tag.getRefCount())
                .build();

        tagDO.initBase();

        return tagDO;
    }

    public static List<Tag> toTags(List<TagDO> tagDOS) {
        List<Tag> tags = new ArrayList<>();

        SafesUtil.ofList(tagDOS).forEach(tagDO -> tags.add(toTag(tagDO)));

        return tags;
    }

    public static Tag toTag(TagDO tagDO) {
        if (ObjectUtils.isEmpty(tagDO)) {
            return null;
        }

        Tag tag = Tag.builder()
                .auditState(AuditStateEn.getEntity(tagDO.getAuditState()))
                .groupName(tagDO.getGroupName())
                .creatorId(tagDO.getCreatorId())
                .description(tagDO.getDescription())
                .name(tagDO.getName())
                .refCount(tagDO.getRefCount())
                .build();
        tag.setId(tagDO.getId());
        tag.setCreatorId(tagDO.getCreatorId());
        tag.setCreateAt(tagDO.getCreateAt());
        tag.setUpdateAt(tagDO.getUpdateAt());

        return tag;
    }

}
