package com.hys.forum.common.transfer;

import com.hys.forum.common.enums.OptLogTypeEn;
import com.hys.forum.pojo.OptLog;
import com.hys.forum.pojo.request.user.UserOptLogPageRequest;
import org.springframework.util.ObjectUtils;


public class OptLogTransfer {

    public static OptLog toOptLog(UserOptLogPageRequest request) {
        return OptLog.builder()
                .operatorId(request.getOperatorId())
                .type(ObjectUtils.isEmpty(request.getType()) ? null : OptLogTypeEn.getEntity(request.getType()))
                .build();
    }

}
