package com.hys.forum.controller;

import com.google.common.collect.Sets;
import com.hys.forum.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.article.ArticleUserPageRequest;
import com.hys.forum.pojo.response.article.ArticleQueryTypesResponse;
import com.hys.forum.pojo.response.article.ArticleUserPageResponse;
import com.hys.forum.pojo.response.config.ConfigResponse;
import com.hys.forum.pojo.response.faq.FaqHotsResponse;
import com.hys.forum.service.api.ArticleApiService;
import com.hys.forum.service.api.ConfigApiService;
import com.hys.forum.service.api.FaqApiService;
import com.hys.forum.common.enums.ConfigTypeEn;
import com.hys.forum.common.config.GlobalViewConfig;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.pojo.request.IndexRequest;
import com.hys.forum.support.WebConst;
import com.hys.forum.support.WebUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hys
 * @desc 首页
 **/
@Slf4j
@Controller
@RequestMapping("/")
public class IndexController {

    private static final String ALL_TYPE_NAME = "全部文章";

    @Resource
    private ArticleApiService articleApiService;

    @Value("${custom-config.view.index-page.sidebar-type-names}")
    private String sideBarTypeNames;

    @Resource
    private FaqApiService faqApiService;

    @Resource
    private WebUtil webUtil;

    @Resource
    private GlobalViewConfig globalViewConfig;

    @Resource
    private ConfigApiService configApiService;

    @GetMapping
    public String index(IndexRequest request, Model model, HttpServletResponse response) {
        //校验是否登录
        if (!ObjectUtils.isEmpty(request.getToken())) {
            WebUtil.cookieAddSid(response, request.getToken());
        }
        request.setType(ObjectUtils.isEmpty(request.getType()) ? ALL_TYPE_NAME : request.getType());

        model.addAttribute("currentDomain", WebConst.DOMAIN_ARTICLE);
        model.addAttribute("toast", request.getToast());
        model.addAttribute("token", request.getToken());

        ResultModel<List<ConfigResponse>> configResult = configApiService.queryAvailable(Sets.newHashSet(ConfigTypeEn.HOME_CAROUSEL.getValue()
                , ConfigTypeEn.SIDEBAR_CAROUSEL.getValue()));
        if (configResult.getSuccess() && !ObjectUtils.isEmpty(configResult.getData())) {
            model.addAttribute("homeCarouselList", webUtil.carouselList(configResult.getData(), ConfigTypeEn.HOME_CAROUSEL));
            model.addAttribute("sideCarouselList", webUtil.carouselList(configResult.getData(), ConfigTypeEn.SIDEBAR_CAROUSEL));
        } else {
            model.addAttribute("homeCarouselList", new ArrayList<>());
            model.addAttribute("sideCarouselList", new ArrayList<>());
        }
        //公告板块
        model.addAttribute("sideBarTypes", sideBarTypes());
        //热门问题列表
        model.addAttribute("hotFaqList", hotFaqList());
        //文章类型列表
        model.addAttribute("typeList", typeList(request));
        //使用的标签
        model.addAttribute("usedTags", webUtil.usedTags());

        // 获取文章列表
        ResultModel<PageResponseModel<ArticleUserPageResponse>> resultModel = userPage(request);
        if (resultModel.getSuccess() && !ObjectUtils.isEmpty(resultModel.getData())) {
            PageResponseModel<ArticleUserPageResponse> pageResponseModel = resultModel.getData();

            model.addAttribute("articleList", webUtil.buildArticles(pageResponseModel.getList()));
            model.addAttribute("pager", pager(request, pageResponseModel));
        } else {
            model.addAttribute("articleList", webUtil.buildArticles(new ArrayList<>()));

            PageResponseModel pageResponseModel = new PageResponseModel();
            pageResponseModel.setTotal(0L);
            model.addAttribute("pager", pager(request, pageResponseModel));
        }

        return "index";
    }

    private ResultModel<PageResponseModel<ArticleUserPageResponse>> userPage(IndexRequest request) {
        PageRequestModel<ArticleUserPageRequest> pageRequestModel = new PageRequestModel<>();
        pageRequestModel.setPageNo(request.getPageNo());
        pageRequestModel.setPageSize(globalViewConfig.getPageSize());
        pageRequestModel.setFilter(ArticleUserPageRequest.builder()
                .typeName(ALL_TYPE_NAME.equals(request.getType()) ? null : request.getType())
                .build());
        return articleApiService.userPage(pageRequestModel);
    }

    private Map<String, Object> pager(IndexRequest request, PageResponseModel pageResponseModel) {
        String queryPath = "?type=" + request.getType() + "&" + WebConst.PAGE_NO_NAME + "=";
        return webUtil.buildPager(request.getPageNo(), queryPath, pageResponseModel);
    }

    private List<Map<String, Object>> hotFaqList() {
        List<Map<String, Object>> postsList = new ArrayList<>(16);

        ResultModel<List<FaqHotsResponse>> resultModel = faqApiService.hots(16);
        if (!resultModel.getSuccess()) {
            return postsList;
        }

        SafesUtil.ofList(resultModel.getData()).forEach(faqHotsResponse -> {
            Map<String, Object> posts = new HashMap<>();
            posts.put("id", faqHotsResponse.getId());
            posts.put("title", faqHotsResponse.getTitle());
            posts.put("createdAt", faqHotsResponse.getCreateAt());
            postsList.add(posts);
        });

        return postsList;
    }

    /**
     * 公告板块
     * @return
     */
    private List<Map<String, Object>> sideBarTypes() {
        List<Map<String, Object>> res = new ArrayList<>();

        if (ObjectUtils.isEmpty(sideBarTypeNames)) {
            return res;
        }

        String[] types = sideBarTypeNames.split(",");
        for (String typeName : types) {
            if (ObjectUtils.isEmpty(typeName)) {
                continue;
            }
            Map<String, Object> type = new HashMap<>();
            type.put("name", typeName);
            type.put("postsList", typeList(typeName));
            res.add(type);
        }

        return res;
    }

    /**
     * 根据类型名查询类型列表
     * @param typeName
     * @return
     */
    private List<Map<String, Object>> typeList(String typeName) {
        List<Map<String, Object>> postsList = new ArrayList<>();
        if (ObjectUtils.isEmpty(typeName)) {
            return postsList;
        }

        PageRequestModel<ArticleUserPageRequest> pageRequestModel = new PageRequestModel<>();
        pageRequestModel.setPageNo(1);
        pageRequestModel.setPageSize(10);
        pageRequestModel.setFilter(ArticleUserPageRequest.builder()
                .typeName(typeName)
                .build());
        ResultModel<PageResponseModel<ArticleUserPageResponse>> resultModel = articleApiService.userPage(pageRequestModel);
        if (!resultModel.getSuccess()) {
            return postsList;
        }

        PageResponseModel<ArticleUserPageResponse> pageResponseModel = resultModel.getData();
        SafesUtil.ofList(pageResponseModel.getList()).forEach(articleUserPageResponse -> {
            Map<String, Object> posts = new HashMap<>(16);
            posts.put("id", articleUserPageResponse.getId());
            posts.put("title", articleUserPageResponse.getTitle());
            posts.put("createdAt", articleUserPageResponse.getCreateAt());
            postsList.add(posts);
        });

        return postsList;
    }

    private List<Map<String, Object>> typeList(IndexRequest request) {
        List<Map<String, Object>> typeList = new ArrayList<>();
        Long allRefCount = 0L;

        Map<String, Object> all = new HashMap<>();
        all.put("name", ALL_TYPE_NAME);
        all.put("selected", request.getType().equals(ALL_TYPE_NAME));
        typeList.add(all);

        ResultModel<List<ArticleQueryTypesResponse>> resultModel = articleApiService.queryAllTypes();
        if (resultModel.getSuccess() && !ObjectUtils.isEmpty(resultModel.getData())) {
            List<ArticleQueryTypesResponse> responses = resultModel.getData();
            for (ArticleQueryTypesResponse response : responses) {
                if (response.getRefCount().equals(0L)) {
                    continue;
                }

                allRefCount += response.getRefCount();

                Map<String, Object> type = new HashMap<>();
                type.put("name", response.getName());
                type.put("refCount", response.getRefCount());
                type.put("selected", request.getType().equals(response.getName()));
                typeList.add(type);
            }
        }

        all.put("refCount", allRefCount);

        return typeList;
    }
}
