package com.hys.forum.controller.rest;

import org.springframework.web.bind.annotation.*;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.article.ArticleSaveArticleRequest;
import com.hys.forum.pojo.response.article.ArticleInfoResponse;
import com.hys.forum.pojo.response.article.ArticleQueryTypesResponse;
import com.hys.forum.service.api.ArticleApiService;
import com.hys.forum.common.constant.Constant;
import com.hys.forum.support.WebUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author hys
 * @desc 文章相关
 **/
@RestController
@RequestMapping("/article-rest")
public class ArticleRestController {

    @Resource
    private ArticleApiService articleApiService;

    @PostMapping("/save")
    public ResultModel<Long> save(@RequestBody ArticleSaveArticleRequest articleRequest, HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));
//        参数输入
        return articleApiService.saveArticle(articleRequest);
    }

    //文章查看详情
    @PostMapping("/{id}")
    public ResultModel<ArticleInfoResponse> get(@PathVariable("id") Long id, HttpServletRequest request) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));
        return articleApiService.info(id);
    }

    @PostMapping("/editArticleTypes")
    public ResultModel<List<ArticleQueryTypesResponse>> getAllType(HttpServletRequest request) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));

        return articleApiService.queryEditArticleTypes();
    }
}
