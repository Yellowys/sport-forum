package com.hys.forum.controller.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.response.tag.TagQueryResponse;
import com.hys.forum.service.api.TagApiService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hys
 * @desc
 **/
@RestController
@RequestMapping("/tag-rest")
public class TagRestController {

    @Resource
    private TagApiService tagApiService;

    @PostMapping("/all")
    public ResultModel<List<TagQueryResponse>> all() {
        return tagApiService.queryAll();
    }
}
