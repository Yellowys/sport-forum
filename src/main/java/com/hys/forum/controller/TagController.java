package com.hys.forum.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.response.tag.TagQueryResponse;
import com.hys.forum.service.api.TagApiService;
import com.hys.forum.pojo.vo.PostsVO;
import com.hys.forum.common.constant.Constant;
import com.hys.forum.common.config.GlobalViewConfig;
import com.hys.forum.pojo.request.BasePageRequest;
import com.hys.forum.common.exception.ViewException;
import com.hys.forum.support.WebConst;
import com.hys.forum.support.WebUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hys
 * @desc
 **/
@Slf4j
@Controller
@RequestMapping("/tag")
public class TagController {

    @Resource
    private TagApiService tagApiService;

    @Resource
    private WebUtil webUtil;

    @Resource
    private GlobalViewConfig globalViewConfig;

    @GetMapping("/{name}")
    public String index(@PathVariable("name") String name, BasePageRequest pageRequest, HttpServletRequest request, Model model) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));

        ResultModel<TagQueryResponse> resultModel = tagApiService.getByName(name);
        if (!resultModel.getSuccess()) {
            throw ViewException.build(resultModel.getMessage());
        }

        TagQueryResponse tagQueryResponse = resultModel.getData();
        model.addAttribute("tag", tag(tagQueryResponse));

        //根据标签id查询其下的帖子列表
        ResultModel<PageResponseModel<PostsVO>> pageResponseModelResultModel = tagPostsList(tagQueryResponse.getId(), pageRequest);
        if (pageResponseModelResultModel.getSuccess() && !ObjectUtils.isEmpty(pageResponseModelResultModel.getData())) {
            PageResponseModel<PostsVO> pageResponseModel = pageResponseModelResultModel.getData();

            model.addAttribute("postsList", webUtil.buildPostsList(pageResponseModel.getList()));
            model.addAttribute("pager", pager(pageRequest, pageResponseModel));
        } else {
            model.addAttribute("postsList", webUtil.buildArticles(new ArrayList<>()));

            PageResponseModel pageResponseModel = new PageResponseModel();
            pageResponseModel.setTotal(0L);
            model.addAttribute("pager", pager(pageRequest, pageResponseModel));
        }

        return "tag-info";
    }

    private Map<String, Object> tag(TagQueryResponse response) {
        Map<String, Object> map = new HashMap<>(16);
        map.put("id", response.getId());
        map.put("name", response.getName());
        map.put("desc", response.getDescription());
        map.put("refCount", response.getRefCount());
        return map;
    }

    private ResultModel<PageResponseModel<PostsVO>> tagPostsList(Long tagId, BasePageRequest pageRequest) {
        PageRequestModel<Long> pageRequestModel = new PageRequestModel<>();
        pageRequestModel.setFilter(tagId);
        pageRequestModel.setPageSize(globalViewConfig.getPageSize());
        pageRequestModel.setPageNo(pageRequest.getPageNo());
        return tagApiService.pagePosts(pageRequestModel);
    }

    private Map<String, Object> pager(BasePageRequest pageRequest, PageResponseModel pageResponseModel) {
        String queryPath = "?" + WebConst.PAGE_NO_NAME + "=";
        return webUtil.buildPager(pageRequest.getPageNo(), queryPath, pageResponseModel);
    }
}
