package com.hys.forum.controller.admin;

import org.springframework.web.bind.annotation.*;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.AdminBooleanRequest;
import com.hys.forum.pojo.request.user.UserAdminPageRequest;
import com.hys.forum.pojo.request.user.UserOptLogPageRequest;
import com.hys.forum.pojo.response.user.UserOptLogPageResponse;
import com.hys.forum.pojo.response.user.UserPageResponse;
import com.hys.forum.service.api.UserApiService;
import com.hys.forum.common.constant.Constant;
import com.hys.forum.support.WebUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author hys
 * @desc
 **/
@RestController
@RequestMapping("/admin-rest/user")
public class AdminRestUserController {

    @Resource
    private UserApiService userApiService;

    @PostMapping("/page")
    public ResultModel<PageResponseModel<UserPageResponse>> add(@RequestBody PageRequestModel<UserAdminPageRequest> pageRequestModel
            , HttpServletRequest request) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));

        return userApiService.adminPage(pageRequestModel);
    }

    //启用用户
    @PostMapping("/enable/{uid}")
    public ResultModel enable(@PathVariable Long uid, HttpServletRequest request) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));

        return userApiService.enable(uid);
    }

    //禁用用户
    @PostMapping("/disable/{uid}")
    public ResultModel disable(@PathVariable Long uid, HttpServletRequest request) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));

        return userApiService.disable(uid);
    }

    /**
     * 操作日志分页
     * @param pageRequestModel
     * @param request
     * @return
     */
    @PostMapping("/page-opt-log")
    public ResultModel<PageResponseModel<UserOptLogPageResponse>> pageOptLog(@RequestBody PageRequestModel<UserOptLogPageRequest> pageRequestModel
            , HttpServletRequest request) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));

        return userApiService.pageOptLog(pageRequestModel);
    }

    /**
     * 设为管理员
     * @param booleanRequest
     * @param request
     * @return
     */
    @PostMapping("/update-role")
    public ResultModel updateRole(@RequestBody AdminBooleanRequest booleanRequest
            , HttpServletRequest request) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));

        return userApiService.updateRole(booleanRequest);
    }
}
