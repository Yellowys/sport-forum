package com.hys.forum.controller.admin;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.AdminBooleanRequest;
import com.hys.forum.pojo.request.tag.TagCreateRequest;
import com.hys.forum.pojo.request.tag.TagPageRequest;
import com.hys.forum.pojo.response.tag.TagPageResponse;
import com.hys.forum.service.api.TagApiService;
import com.hys.forum.common.constant.Constant;
import com.hys.forum.support.WebUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
/**
 * @author hys
 * @desc
 **/
@RestController
@RequestMapping("/admin-rest/tag")
public class AdminRestTagController {

    @Resource
    private TagApiService tagApiService;

    /**
     * 标签分页
     * @param pageRequestModel
     * @param request
     * @return
     */
    @PostMapping("/page")
    public ResultModel<PageResponseModel<TagPageResponse>> page(@RequestBody PageRequestModel<TagPageRequest> pageRequestModel
            , HttpServletRequest request) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));

        return tagApiService.page(pageRequestModel);
    }

    /**
     * 修改标签状态
     * @param booleanRequest
     * @param request
     * @return
     */
    @PostMapping("/audit-state")
    public ResultModel auditState(@RequestBody AdminBooleanRequest booleanRequest, HttpServletRequest request) {
        request.setAttribute(Constant.REQUEST_HEADER_TOKEN_KEY, WebUtil.cookieGetSid(request));

        return tagApiService.auditState(booleanRequest);
    }

    /**
     * 新增标签
     * @param request
     * @return
     */
    @PostMapping("/add")
    public ResultModel add(@RequestBody TagCreateRequest request) {
        return tagApiService.create(request);
    }

}
