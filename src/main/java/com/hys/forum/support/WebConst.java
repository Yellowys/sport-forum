package com.hys.forum.support;

/**
 * @author hys
 * @desc
 **/
public interface WebConst {

    String PAGE_NO_NAME = "pageNo";
//    Integer PAGE_SIZE = 10;

    String DOMAIN_ARTICLE = "article";
    String DOMAIN_FAQ = "faq";
    //关注
    String DOMAIN_INTEREST = "interest";

    String COOKIE_SID_KEY = "sid";

    String REQUEST_REDIRECT_PREFIX = "redirect:";
}
