package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import com.hys.forum.support.Pair;
import com.hys.forum.common.enums.*;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.BasePosts;
import com.hys.forum.pojo.Message;
import com.hys.forum.pojo.value.IdValue;
import com.hys.forum.service.MessageService;
import com.hys.forum.service.PostsService;

import javax.annotation.Resource;


@Component
public class MessageApprovalCreateListener extends EventBus.EventHandler<Pair<Long>> {

    @Resource
    private MessageService messageService;

    @Resource
    private PostsService postsService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.APPROVAL_CREATE;
    }

    @Override
    public void onMessage(Pair<Long> pair) {
        Long userId = pair.getValue0();
        Long postsId = pair.getValue1();

        BasePosts basePosts = postsService.get(postsId);
        if (ObjectUtils.isEmpty(basePosts)) {
            return;
        }

        Message message = Message.builder()
                .channel(MessageChannelEn.STATION_LETTER)
                .receiver(IdValue.builder()
                        .id(basePosts.getAuthorId().toString())
                        .type(IdValueTypeEn.USER_ID)
                        .build())
                .read(MessageReadEn.NO)
                .contentType(MessageContentTypeEn.TEXT)
                .title(postsId.toString())
                .content("")
                .sender(IdValue.builder()
                        .id(userId.toString())
                        .type(IdValueTypeEn.USER_ID)
                        .build())
                .build();
        if (PostsCategoryEn.ARTICLE.equals(basePosts.getCategory())) {
            message.setType(MessageTypeEn.APPROVAL_ARTICLE);
        } else if (PostsCategoryEn.FAQ.equals(basePosts.getCategory())) {
            message.setType(MessageTypeEn.APPROVAL_FAQ);
        } else {
            return;
        }

        messageService.save(message);
    }
}
