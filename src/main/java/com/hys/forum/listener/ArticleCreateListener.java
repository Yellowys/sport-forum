package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Article;
import com.hys.forum.pojo.Tag;
import com.hys.forum.service.ArticleTypeService;
import com.hys.forum.service.TagService;

import javax.annotation.Resource;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class ArticleCreateListener extends EventBus.EventHandler<Article> {

    @Resource
    private TagService tagService;

    @Resource
    private ArticleTypeService articleTypeService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.ARTICLE_CREATE;
    }

    @Override
    public void onMessage(Article article) {
        Set<Long> tagIds = article.getTags().stream().map(Tag::getId).collect(Collectors.toSet());
        //更新标签引用数
        tagService.increaseRefCount(tagIds);

        //更新新文章的类型的总数
        articleTypeService.increaseRefCount(article.getType().getId());
    }
}
