package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.enums.SearchTypeEn;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Faq;
import com.hys.forum.pojo.Search;
import com.hys.forum.service.SearchService;

import javax.annotation.Resource;

@Component
public class SearchFaqCreateListener extends EventBus.EventHandler<Faq> {

    @Resource
    private SearchService searchService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.FAQ_CREATE;
    }

    @Override
    public void onMessage(Faq faq) {
        searchService.deleteByPostsId(faq.getId());

        searchService.save(Search.builder()
                .content(faq.getMarkdownContent())
                .entityId(faq.getId())
                .title(faq.getTitle())
                .type(SearchTypeEn.POSTS)
                .build());
    }
}
