package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.support.Pair;
import com.hys.forum.common.enums.SearchTypeEn;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Article;
import com.hys.forum.pojo.Search;
import com.hys.forum.service.SearchService;

import javax.annotation.Resource;


@Component
public class SearchArticleUpdateListener  extends EventBus.EventHandler<Pair<Article>> {

    @Resource
    private SearchService searchService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.ARTICLE_UPDATE;
    }

    @Override
    public void onMessage(Pair<Article> pair) {
        Article newArticle = pair.getValue1();

        searchService.deleteByPostsId(newArticle.getId());

        searchService.save(Search.builder()
                .content(newArticle.getMarkdownContent())
                .entityId(newArticle.getId())
                .title(newArticle.getTitle())
                .type(SearchTypeEn.POSTS)
                .build());
    }
}
