package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import com.hys.forum.support.Pair;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.UserFeed;
import com.hys.forum.service.PostsService;
import com.hys.forum.service.UserFeedService;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class FeedUserFollowListener extends EventBus.EventHandler<Pair<Long>> {

    @Resource
    private PostsService postsService;

    @Resource
    private UserFeedService userFeedService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.USER_FOLLOW;
    }

    @Override
    public void onMessage(Pair<Long> pair) {
        Long followed = pair.getValue0();
        Long follower = pair.getValue1();

        List<Long> postsIds = postsService.getAllIdByAuthorId(followed);
        if (ObjectUtils.isEmpty(postsIds)) {
            return;
        }

        List<UserFeed> userFeeds = postsIds.stream().map(postsId -> {
            return UserFeed.builder()
                    .postsId(postsId)
                    .userId(follower)
                    .build();
        }).collect(Collectors.toList());

        userFeedService.batchSave(userFeeds);
    }
}
