package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Faq;
import com.hys.forum.pojo.Tag;
import com.hys.forum.service.TagService;

import javax.annotation.Resource;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class FaqCreateListener extends EventBus.EventHandler<Faq> {

    @Resource
    private TagService tagService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.FAQ_CREATE;
    }

    @Override
    public void onMessage(Faq faq) {
        Set<Long> tagIds = faq.getTags().stream().map(Tag::getId).collect(Collectors.toSet());
        //更新问答的标签引用数
        tagService.increaseRefCount(tagIds);
    }
}
