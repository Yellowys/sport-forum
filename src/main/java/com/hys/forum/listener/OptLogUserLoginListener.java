package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.OptLog;
import com.hys.forum.service.OptLogService;

import javax.annotation.Resource;


@Component
public class OptLogUserLoginListener extends EventBus.EventHandler<OptLog> {

    @Resource
    private OptLogService optLogService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.USER_LOGIN;
    }

    @Override
    public void onMessage(OptLog optLog) {
        optLogService.save(optLog);
    }
}
