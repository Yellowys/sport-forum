package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.support.Pair;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.UserFeed;
import com.hys.forum.service.UserFeedService;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Component
public class FeedApprovalCreateListener extends EventBus.EventHandler<Pair<Long>> {

    @Resource
    private UserFeedService userFeedService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.APPROVAL_CREATE;
    }

    @Override
    public void onMessage(Pair<Long> pair) {
        Long userId = pair.getValue0();
        Long postsId = pair.getValue1();

        List<UserFeed> userFeeds = Arrays.asList(UserFeed.builder()
                .postsId(postsId)
                .userId(userId)
                .build());

        userFeedService.batchSave(userFeeds);
    }
}
