package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.support.Pair;
import com.hys.forum.common.enums.*;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Message;
import com.hys.forum.pojo.value.IdValue;
import com.hys.forum.service.MessageService;

import javax.annotation.Resource;


@Component
public class MessageUserFollowListener extends EventBus.EventHandler<Pair<Long>> {

    @Resource
    private MessageService messageService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.USER_FOLLOW;
    }

    @Override
    public void onMessage(Pair<Long> pair) {
        Long followed = pair.getValue0();
        Long follower = pair.getValue1();

        messageService.save(Message.builder()
                .channel(MessageChannelEn.STATION_LETTER)
                .type(MessageTypeEn.FOLLOW_USER)
                .receiver(IdValue.builder()
                        .id(followed.toString())
                        .type(IdValueTypeEn.USER_ID)
                        .build())
                .read(MessageReadEn.NO)
                .contentType(MessageContentTypeEn.TEXT)
                .content("")
                .sender(IdValue.builder()
                        .id(follower.toString())
                        .type(IdValueTypeEn.USER_ID)
                        .build())
                .title("")
                .build());
    }
}
