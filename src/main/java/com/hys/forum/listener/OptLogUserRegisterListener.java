package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.OptLog;
import com.hys.forum.pojo.User;
import com.hys.forum.service.OptLogService;

import javax.annotation.Resource;

@Component
public class OptLogUserRegisterListener extends EventBus.EventHandler<User> {

    @Resource
    private OptLogService optLogService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.USER_REGISTER;
    }

    @Override
    public void onMessage(User user) {

        // 保存操作记录
        optLogService.save(OptLog.createUserRegisterRecordLog(user.getId(), user));
    }
}
