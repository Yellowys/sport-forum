package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.BasePosts;
import com.hys.forum.service.PostsService;

import javax.annotation.Resource;


@Component
public class PostsInfoListener extends EventBus.EventHandler<BasePosts> {

    @Resource
    private PostsService postsService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.POSTS_INFO;
    }

    @Override
    public void onMessage(BasePosts posts) {
        postsService.increaseViews(posts.getId(), posts.getUpdateAt());
    }
}
