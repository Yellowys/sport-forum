package com.hys.forum.listener;

import com.hys.forum.service.UserFollowService;
import com.hys.forum.service.UserFeedService;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import com.hys.forum.common.enums.FollowedTypeEn;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Faq;
import com.hys.forum.pojo.UserFeed;
import com.hys.forum.service.UserFollowService;
import com.hys.forum.service.UserFeedService;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class FeedFaqCreateListener extends EventBus.EventHandler<Faq> {

    @Resource
    private UserFollowService userFollowService;

    @Resource
    private UserFeedService userFeedService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.FAQ_CREATE;
    }

    @Override
    public void onMessage(Faq faq) {
        //谁关注了作者，把作者的新问答写进关注表
        List<Long> followedIds = userFollowService.getAllFollowerIds(faq.getAuthorId(), FollowedTypeEn.USER);
        if (ObjectUtils.isEmpty(followedIds)) {
            return;
        }

        List<UserFeed> userFeeds = followedIds.stream().map(userId -> {
            return UserFeed.builder()
                    .postsId(faq.getId())
                    .userId(userId)
                    .build();
        }).collect(Collectors.toList());

        userFeedService.batchSave(userFeeds);
    }
}
