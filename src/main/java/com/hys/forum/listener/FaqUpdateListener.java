package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import com.hys.forum.support.Pair;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Faq;
import com.hys.forum.service.TagService;

import javax.annotation.Resource;
import java.util.Set;

@Component
public class FaqUpdateListener extends EventBus.EventHandler<Pair<Faq>> {

    @Resource
    private TagService tagService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.FAQ_UPDATE;
    }

    @Override
    public void onMessage(Pair<Faq> pair) {
        Faq oldFaq = pair.getValue0();
        Faq newFaq = pair.getValue1();

        // 由于FaqManager里已经减了一遍标签的引用计数，需把原来的加回来
        Set<Long> oldTags=Pair.tagToLong(oldFaq.getTags());
        tagService.increaseRefCount(oldTags);

        // 更新标签引用计数
        Set<Long> addTags = Pair.diff(newFaq.getTags(), oldFaq.getTags());
        Set<Long> removeTags = Pair.diff(oldFaq.getTags(), newFaq.getTags());
        if (!ObjectUtils.isEmpty(addTags)) {
            tagService.increaseRefCount(addTags);
        }
        if (!ObjectUtils.isEmpty(removeTags)) {
            tagService.decreaseRefCount(removeTags);
        }
    }

}
