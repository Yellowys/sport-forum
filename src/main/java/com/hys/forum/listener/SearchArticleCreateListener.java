package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.enums.SearchTypeEn;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Article;
import com.hys.forum.pojo.Search;
import com.hys.forum.service.SearchService;

import javax.annotation.Resource;


@Component
public class SearchArticleCreateListener extends EventBus.EventHandler<Article> {

    @Resource
    private SearchService searchService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.ARTICLE_CREATE;
    }

    @Override
    public void onMessage(Article article) {
        searchService.deleteByPostsId(article.getId());

        searchService.save(Search.builder()
                .content(article.getMarkdownContent())
                .entityId(article.getId())
                .title(article.getTitle())
                .type(SearchTypeEn.POSTS)
                .build());
    }
}
