package com.hys.forum.listener;

import com.hys.forum.service.UserFollowService;
import com.hys.forum.service.UserFeedService;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import com.hys.forum.common.enums.FollowedTypeEn;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Article;
import com.hys.forum.pojo.UserFeed;
import com.hys.forum.service.UserFollowService;
import com.hys.forum.service.UserFeedService;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FeedArticleCreateListener extends EventBus.EventHandler<Article> {

    @Resource
    private UserFollowService userFollowService;

    @Resource
    private UserFeedService userFeedService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.ARTICLE_CREATE;
    }

    @Override
    public void onMessage(Article article) {
        List<Long> followedIds = userFollowService.getAllFollowerIds(article.getAuthorId(), FollowedTypeEn.USER);
        if (ObjectUtils.isEmpty(followedIds)) {
            return;
        }

        List<UserFeed> userFeeds = followedIds.stream().map(userId -> {
            return UserFeed.builder()
                    .postsId(article.getId())
                    .userId(userId)
                    .build();
        }).collect(Collectors.toList());

        userFeedService.batchSave(userFeeds);
    }
}
