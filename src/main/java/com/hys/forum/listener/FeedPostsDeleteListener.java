package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.BasePosts;
import com.hys.forum.service.UserFeedService;

import javax.annotation.Resource;


@Component
public class FeedPostsDeleteListener extends EventBus.EventHandler<BasePosts> {

    @Resource
    private UserFeedService userFeedService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.POSTS_DELETE;
    }

    @Override
    public void onMessage(BasePosts basePosts) {
        userFeedService.deleteByPostsId(basePosts.getId());
    }
}
