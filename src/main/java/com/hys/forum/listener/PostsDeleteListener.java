package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.enums.PostsCategoryEn;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.BasePosts;
import com.hys.forum.service.ArticleTypeService;

import javax.annotation.Resource;


@Component
public class PostsDeleteListener extends EventBus.EventHandler<BasePosts> {

    @Resource
    private ArticleTypeService articleTypeService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.POSTS_DELETE;
    }

    @Override
    public void onMessage(BasePosts basePosts) {
        // 文章类别引用数减
        if (PostsCategoryEn.ARTICLE.equals(basePosts.getCategory())) {
            articleTypeService.decreaseRefCount(basePosts.getTypeId());
        }
    }
}
