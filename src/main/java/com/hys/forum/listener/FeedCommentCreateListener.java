package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Comment;
import com.hys.forum.pojo.UserFeed;
import com.hys.forum.service.UserFeedService;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Component
public class FeedCommentCreateListener extends EventBus.EventHandler<Map<String, Object>> {

    @Resource
    private UserFeedService userFeedService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.COMMENT_CREATE;
    }

    @Override
    public void onMessage(Map<String, Object> msg) {
        Long userId = Long.valueOf(msg.get("commenter").toString());
        Comment comment = (Comment) msg.get("comment");

        List<UserFeed> userFeeds = Arrays.asList(UserFeed.builder()
                .postsId(comment.getPostsId())
                .userId(userId)
                .build());

        userFeedService.batchSave(userFeeds);
    }
}
