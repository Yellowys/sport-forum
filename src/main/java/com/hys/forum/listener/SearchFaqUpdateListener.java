package com.hys.forum.listener;

import org.springframework.stereotype.Component;
import com.hys.forum.support.Pair;
import com.hys.forum.common.enums.SearchTypeEn;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Faq;
import com.hys.forum.pojo.Search;
import com.hys.forum.service.SearchService;

import javax.annotation.Resource;


@Component
public class SearchFaqUpdateListener extends EventBus.EventHandler<Pair<Faq>> {

    @Resource
    private SearchService searchService;

    @Override
    public EventBus.Topic topic() {
        return EventBus.Topic.FAQ_UPDATE;
    }

    @Override
    public void onMessage(Pair<Faq> pair) {
        Faq newFaq = pair.getValue1();

        searchService.deleteByPostsId(newFaq.getId());

        searchService.save(Search.builder()
                .content(newFaq.getMarkdownContent())
                .entityId(newFaq.getId())
                .title(newFaq.getTitle())
                .type(SearchTypeEn.POSTS)
                .build());
    }
}
