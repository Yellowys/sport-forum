package com.hys.forum.pojo.response.article;

import com.hys.forum.pojo.vo.TagVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ArticleUserPageResponse implements Serializable {

    private Long id;

    private String auditState;

    private String category;

    private String categoryDesc;

    private Boolean official;

    private Boolean top;

    private Boolean marrow;

    private String title;

    private String introduction;

    private String headImg;

    private Long authorId;

    private String authorNickname;

    private String authorAvatar;

    private Date createAt;

    private Date updateAt;

    private Long views;

    private Long approvals;

    private Long comments;

    private List<TagVO> tags;

}
