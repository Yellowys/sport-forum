package com.hys.forum.pojo.response.faq;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FaqHotsResponse implements Serializable {

    private Long id;

    private String title;

    private Date createAt;
}
