package com.hys.forum.pojo.request;

import lombok.Data;

/**
 * @author hys
 * @desc
 **/
@Data
public class IndexRequest extends BasePageRequest {

    private String type;

    private String toast;

    private String token;
}
