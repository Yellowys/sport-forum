package com.hys.forum.pojo.request;

import lombok.Data;

/**
 * @author hys
 * @desc
 **/
@Data
public class SearchRequest extends BasePageRequest {

    private String key;
}
