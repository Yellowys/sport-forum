package com.hys.forum.pojo.request;

import lombok.Data;

/**
 * @author hys
 * @desc
 **/
@Data
public class UserRequest extends BasePageRequest {

    private String type;

}
