package com.hys.forum.pojo.request;

import lombok.Data;

/**
 * @author hys
 * @desc
 **/
@Data
public class BasePageRequest {

    private int pageNo = 1;

}
