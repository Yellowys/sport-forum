package com.hys.forum.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserFeed extends BaseEntity {

    private Long userId;

    private Long postsId;

}
