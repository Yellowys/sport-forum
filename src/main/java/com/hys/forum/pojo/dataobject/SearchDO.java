package com.hys.forum.pojo.dataobject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hys
 * @desc
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchDO extends BaseDO {

    private String type;

    private Long entityId;

    private String title;

    private String content;

}
