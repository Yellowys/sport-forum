package com.hys.forum.manager;

import com.alibaba.fastjson.JSON;
import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.common.exception.ViewException;
import com.hys.forum.pojo.response.user.UserInfoResponse;
import com.hys.forum.pojo.response.user.UserOptLogPageResponse;
import com.hys.forum.pojo.response.user.UserPageResponse;
import com.hys.forum.support.*;
import com.hys.forum.common.transfer.OptLogTransfer;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.request.AdminBooleanRequest;
import com.hys.forum.pojo.request.user.*;
import com.hys.forum.common.transfer.UserTransfer;
import com.hys.forum.common.enums.CacheBizTypeEn;
import com.hys.forum.common.enums.ErrorCodeEn;
import com.hys.forum.common.enums.UserRoleEn;
import com.hys.forum.common.enums.UserStateEn;
import com.hys.forum.common.exception.BizException;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.common.support.*;
import com.hys.forum.pojo.Follow;
import com.hys.forum.pojo.OptLog;
import com.hys.forum.pojo.User;
import com.hys.forum.service.OptLogService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;



@Component
public class UserManager extends AbstractLoginManager {

    @Resource
    private OptLogService optLogService;

    public static void isEmail(String email){
        String regEx1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern p;
        Matcher m;
        p = Pattern.compile(regEx1);
        m = p.matcher(email);
        if(!m.matches()){
            throw new BizException(ErrorCodeEn.NOT_EMAIL);
        }
    }


    /**
     * 邮箱 + 密码 登录
     * @param request
     * @return
     */
    public String emailRequestLogin(UserEmailLoginRequest request) {
        //判断邮箱是否符合邮箱规范
        isEmail(request.getEmail());

        // 判断邮箱是否存在
        User user = userService.getByEmail(request.getEmail());
        CheckUtil.isEmpty(user, ErrorCodeEn.USER_NOT_EXIST);
        CheckUtil.isTrue(UserStateEn.DISABLE.equals(user.getState()), ErrorCodeEn.USER_STATE_IS_DISABLE);

        // 判断登录密码是否正确
        CheckUtil.isFalse(StringUtil.md5UserPassword(request.getPassword()).equals(user.getPassword()), ErrorCodeEn.USER_LOGIN_PWD_ERROR);

        // 更新最后登录时间
        user.setLastLoginTime(new Date());
        userService.update(user);

        return login(user, request);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public PageResponseModel<UserOptLogPageResponse> pageOptLog(PageRequestModel<UserOptLogPageRequest> pageRequestModel) {
        PageResult<OptLog> pageResult = optLogService.page(PageUtil.buildPageRequest(pageRequestModel, OptLogTransfer.toOptLog(pageRequestModel.getFilter())));

        if (ObjectUtils.isEmpty(pageResult.getList())) {
            return PageUtil.buildPageResponseModel(pageResult, new ArrayList<>());
        }
        List<Long> userIdList = pageResult.getList().stream().map(OptLog::getOperatorId).collect(Collectors.toList());
        List<User> userList = userService.queryByIds(userIdList);

        return PageUtil.buildPageResponseModel(pageResult, UserTransfer.toUserOptLogPageResponses(pageResult.getList(), userList));
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void enable(Long uid) {
        User user = userService.get(uid);
        CheckUtil.isEmpty(user, ErrorCodeEn.USER_NOT_EXIST);

        user.setState(UserStateEn.ENABLE);
        userService.update(user);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void disable(Long uid) {
        User user = userService.get(uid);
        CheckUtil.isEmpty(user, ErrorCodeEn.USER_NOT_EXIST);
        CheckUtil.isEmpty(UserRoleEn.SUPER_ADMIN.equals(user.getRole()), ErrorCodeEn.COMMON_TOKEN_NO_PERMISSION);

        User loginUser = LoginUserContext.getUser();
        if(loginUser.getId() == user.getId()){
            throw ViewException.build("无权限");
        }
        user.setState(UserStateEn.DISABLE);
        userService.update(user);

        // 删除用户登录信息
        deleteLoginUser(uid);
    }

    @IsLogin
    public void follow(Long followed) {
        Long follower = LoginUserContext.getUser().getId();
        userService.follow(followed, follower);

        Pair<Long> follow = Pair.build(followed, follower);
        EventBus.emit(EventBus.Topic.USER_FOLLOW, follow);
    }

    @IsLogin
    public void cancelFollow(Long followed) {
        Follow follow = userService.getFollow(followed, LoginUserContext.getUser().getId());
        if (follow == null) {
            return;
        }
        userService.cancelFollow(follow.getId());

        EventBus.emit(EventBus.Topic.USER_CANCEL_FOLLOW, follow);
    }

    /**
     * 获取 token 对应用户详情
     * @param token
     * @return
     */
    public UserInfoResponse info(String token) {
        String cacheUserStr = cacheService.get(CacheBizTypeEn.USER_LOGIN_TOKEN, token);
        CheckUtil.isEmpty(cacheUserStr, ErrorCodeEn.USER_TOKEN_INVALID);

        return UserTransfer.toUserInfoResponse(JSON.parseObject(cacheUserStr, User.class));
    }

    public UserInfoResponse info(Long uid) {
        User user = userService.get(uid);
        CheckUtil.isEmpty(user, ErrorCodeEn.USER_NOT_EXIST);

        return UserTransfer.toUserInfoResponse(user);
    }

    /**
     * 用户注册
     * @param request
     */
    @Transactional
    public String register(UserRegisterRequest request) {
        // 判断邮箱是否已经被注册
        User user = userService.getByEmail(request.getEmail());
        CheckUtil.isNotEmpty(user, ErrorCodeEn.USER_REGISTER_EMAIL_IS_EXIST);

        //在转换时进行密码的加密
        User registerUser = UserTransfer.toUser(request);

        // 保存注册用户
        userService.save(registerUser);

        // 触发保存操作日志事件
        EventBus.emit(EventBus.Topic.USER_REGISTER, registerUser);

        return login(registerUser, request);
    }

    /**
     * 登出
     * @param request
     */
    public void logout(UserTokenLogoutRequest request) {
        User user = delCacheLoginUser(request.getToken());
        if (ObjectUtils.isEmpty(user)) {
            return;
        }

        // 触发保存操作日志事件
        EventBus.emit(EventBus.Topic.USER_LOGOUT, OptLog.createUserLogoutRecordLog(user.getId(), JSON.toJSONString(request)));
    }

    /**
     * 用户更新基本信息
     * @param request
     */
    @IsLogin
    @Transactional
    public void updateInfo(UserUpdateInfoRequest request) {
        User loginUser = LoginUserContext.getUser();

        User user = userService.getByEmail(request.getEmail());
        if (!ObjectUtils.isEmpty(user)) {
            CheckUtil.isFalse(user.getId().equals(loginUser.getId()), ErrorCodeEn.USER_REGISTER_EMAIL_IS_EXIST);
        }

        User updateUser = UserTransfer.toUser(loginUser, request);

        // 更新缓存中登录用户信息
        updateCacheUser(updateUser);
        userService.update(updateUser);
    }


    /**
     * 用户更新头像
     * @param linkFilenameData
     */
    @IsLogin
    @Transactional
    public void updateHeadimg(String linkFilenameData) {
        User loginUser = LoginUserContext.getUser();

       /* User user = userService.getByEmail(request.getEmail());
        if (!ObjectUtils.isEmpty(user)) {
            CheckUtil.isFalse(user.getId().equals(loginUser.getId()), ErrorCodeEn.USER_REGISTER_EMAIL_IS_EXIST);
        }*/
        loginUser.setAvatar(linkFilenameData);
        // 更新缓存中登录用户信息
        updateCacheUser(loginUser);
        userService.update(loginUser);
    }
    /**
     * 更新登录密码
     * @param request
     */
    @IsLogin
    @Transactional
    public void updatePwd(UserUpdatePwdRequest request) {
        User user = LoginUserContext.getUser();
        CheckUtil.isFalse(StringUtil.md5UserPassword(request.getOldPassword()).equals(user.getPassword()), ErrorCodeEn.USER_OLD_PASSWORD_ERROR);

        user.setPassword(StringUtil.md5UserPassword(request.getNewPassword()));

        // 更新缓存中登录用户信息
        updateCacheUser(user);
        userService.update(user);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public PageResponseModel<UserPageResponse> page(PageRequestModel<UserAdminPageRequest> pageRequestModel) {
        PageResult<User> pageResult = userService.page(PageUtil.buildPageRequest(pageRequestModel, UserTransfer.toUser(pageRequestModel.getFilter())));

        return PageUtil.buildPageResponseModel(pageResult, UserTransfer.toUserPageResponses(pageResult.getList()));
    }

    /**
     * 用户关注列表
     * @param pageRequestModel
     * @return
     */
    public PageResponseModel<UserPageResponse> pageFollower(PageRequestModel<Long> pageRequestModel) {
        PageResult<User> pageResult = userService.pageFollower(PageUtil.buildPageRequest(pageRequestModel, pageRequestModel.getFilter()));

        return PageUtil.buildPageResponseModel(pageResult, UserTransfer.toUserPageResponses(pageResult.getList()));
    }

    /**
     * 用户粉丝
     * @param pageRequestModel
     * @return
     */
    public PageResponseModel<UserPageResponse> pageFans(PageRequestModel<Long> pageRequestModel) {
        PageResult<User> pageResult = userService.pageFans(PageUtil.buildPageRequest(pageRequestModel, pageRequestModel.getFilter()));

        return PageUtil.buildPageResponseModel(pageResult, UserTransfer.toUserPageResponses(pageResult.getList()));
    }

    @IsLogin
    public Boolean hasFollow(Long followed) {
        Follow follow = userService.getFollow(followed, LoginUserContext.getUser().getId());
        return follow != null;
    }

    public PageResponseModel<UserPageResponse> pageActive(PageRequestModel pageRequestModel) {
        PageResult<User> pageResult = userService.pageActive(PageUtil.buildPageRequest(pageRequestModel));

        return PageUtil.buildPageResponseModel(pageResult, UserTransfer.toUserPageResponses(pageResult.getList()));
    }

    private void updateCacheUser(User updateUser) {
        LoginUserContext.setUser(updateUser);
        cacheLoginUser(LoginUserContext.getToken(), updateUser);
    }

    private User delCacheLoginUser(String token) {
        String oldUser = cacheService.get(CacheBizTypeEn.USER_LOGIN_TOKEN, token);
        if (ObjectUtils.isEmpty(oldUser)) {
            return null;
        }

        User loginUser = JSON.parseObject(oldUser, User.class);
        cacheService.del(CacheBizTypeEn.USER_LOGIN_TOKEN, String.valueOf(loginUser.getId()));
        cacheService.del(CacheBizTypeEn.USER_LOGIN_TOKEN, token);

        return loginUser;
    }


    @IsLogin(role = UserRoleEn.SUPER_ADMIN)
    public void updateRole(AdminBooleanRequest booleanRequest) {
        User user = userService.get(booleanRequest.getId());
        CheckUtil.isEmpty(user, ErrorCodeEn.USER_NOT_EXIST);

        user.setRole(booleanRequest.getIs() ? UserRoleEn.ADMIN : UserRoleEn.USER);
        userService.update(user);
    }
}
