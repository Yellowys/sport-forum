package com.hys.forum.manager;

import org.springframework.stereotype.Component;
import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.support.LoginUserContext;
import com.hys.forum.support.Pair;
import com.hys.forum.common.enums.ErrorCodeEn;
import com.hys.forum.common.support.CheckUtil;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.Approval;
import com.hys.forum.pojo.BasePosts;
import com.hys.forum.service.ApprovalService;
import com.hys.forum.service.PostsService;

import javax.annotation.Resource;


@Component
public class ApprovalManager {

    @Resource
    private ApprovalService approvalService;

    @Resource
    private PostsService postsService;

    @IsLogin
    public Long create(Long postsId) {
        //查询是否已关注
        Approval approval = approvalService.get(postsId, LoginUserContext.getUser().getId());
        CheckUtil.isNotEmpty(approval, ErrorCodeEn.REPEAT_OPERATION);

        BasePosts basePosts = postsService.get(postsId);
        CheckUtil.isEmpty(basePosts, ErrorCodeEn.POSTS_NOT_EXIST);

        approvalService.save(Approval.builder()
                .postsId(postsId)
                .userId(LoginUserContext.getUser().getId())
                .build());
        postsService.increaseApproval(postsId, basePosts.getUpdateAt());

        EventBus.emit(EventBus.Topic.APPROVAL_CREATE, Pair.build(LoginUserContext.getUser().getId(), postsId));
        //点赞数加1
        return basePosts.getApprovals() + 1;
    }

    @IsLogin
    public Long delete(Long postsId) {
        Approval approval = approvalService.get(postsId, LoginUserContext.getUser().getId());
        CheckUtil.isEmpty(approval, ErrorCodeEn.OPERATION_DATA_NOT_EXIST);

        BasePosts basePosts = postsService.get(postsId);
        CheckUtil.isEmpty(basePosts, ErrorCodeEn.POSTS_NOT_EXIST);

        approvalService.delete(approval.getId());
        postsService.decreaseApproval(postsId, basePosts.getUpdateAt());

        return basePosts.getApprovals() - 1;
    }

    @IsLogin
    public Boolean hasApproval(Long postsId) {
        Approval approval = approvalService.get(postsId, LoginUserContext.getUser().getId());

        return approval != null;
    }

}
