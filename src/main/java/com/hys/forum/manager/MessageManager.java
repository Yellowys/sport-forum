package com.hys.forum.manager;

import com.hys.forum.pojo.response.message.MessagePageResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.request.message.MessagePageRequest;
import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.support.LoginUserContext;
import com.hys.forum.support.PageUtil;
import com.hys.forum.common.transfer.MessageTransfer;
import com.hys.forum.common.enums.ErrorCodeEn;
import com.hys.forum.common.enums.MessageChannelEn;
import com.hys.forum.common.enums.MessageReadEn;
import com.hys.forum.common.enums.MessageTypeEn;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.common.support.CheckUtil;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.pojo.BasePosts;
import com.hys.forum.pojo.Message;
import com.hys.forum.pojo.User;
import com.hys.forum.pojo.value.IdValue;
import com.hys.forum.service.MessageService;
import com.hys.forum.service.PostsService;
import com.hys.forum.service.UserService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class MessageManager {

    @Resource
    private MessageService messageService;

    @Resource
    private UserService userService;

    @Resource
    private PostsService postsService;

    @IsLogin
    public PageResponseModel<MessagePageResponse> page(PageRequestModel<MessagePageRequest> pageRequestModel) {
        MessagePageRequest pageRequest = pageRequestModel.getFilter();
        Message message = Message.builder()
                .channel(MessageChannelEn.STATION_LETTER)
                .receiver(IdValue.builder()
                        .id(LoginUserContext.getUser().getId().toString())
                        .build())
                .type(MessageTypeEn.getEntityByDesc(pageRequest.getTypeDesc()))
                .build();
        PageResult<Message> pageResult = messageService.page(PageUtil.buildPageRequest(pageRequestModel, message));
        if (ObjectUtils.isEmpty(pageResult.getList())) {
            return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), new ArrayList<>());
        }

        Set<Long> userIds = new HashSet<>();
        Set<Long> postsIds = new HashSet<>();
        SafesUtil.ofList(pageResult.getList()).forEach(message1 -> {
            userIds.add(Long.valueOf(message1.getSender().getId()));

            if (MessageTypeEn.APPROVAL_ARTICLE.equals(message.getType())
                    || MessageTypeEn.APPROVAL_FAQ.equals(message.getType())
                    || MessageTypeEn.COMMENT_FAQ.equals(message.getType())
                    || MessageTypeEn.COMMENT_ARTICLE.equals(message.getType())) {
                postsIds.add(Long.valueOf(message1.getTitle()));
            }
        });
        List<User> users = userService.queryByIds(new ArrayList<>(userIds));

        List<BasePosts> postsList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(postsIds)) {
            postsList = postsService.queryInIds(postsIds);
        }

        List<MessagePageResponse> responses = MessageTransfer.toMessagePageResponses(pageResult.getList(), users, postsList, LoginUserContext.getUser());
        return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), responses);
    }

    @IsLogin
    public void markIsRead(Long messageId) {
        Message message = messageService.get(messageId);
        CheckUtil.isEmpty(message, ErrorCodeEn.MESSAGE_NOT_EXIST);
        CheckUtil.isFalse(LoginUserContext.getUser().getId().toString().equals(message.getReceiver().getId()), ErrorCodeEn.MESSAGE_NOT_EXIST);

        message.setRead(MessageReadEn.YES);
        messageService.updateToRead(message);
    }

    @IsLogin
    public Long countUnRead() {
        return messageService.countUnRead(LoginUserContext.getUser().getId());
    }
}
