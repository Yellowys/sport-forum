package com.hys.forum.manager;

import com.hys.forum.pojo.response.config.ConfigResponse;
import org.springframework.stereotype.Component;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.request.AdminBooleanRequest;
import com.hys.forum.pojo.request.config.ConfigAddRequest;
import com.hys.forum.pojo.request.config.ConfigPageRequest;
import com.hys.forum.pojo.request.config.ConfigUpdateRequest;
import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.support.LoginUserContext;
import com.hys.forum.support.PageUtil;
import com.hys.forum.common.transfer.ConfigTransfer;
import com.hys.forum.common.enums.AuditStateEn;
import com.hys.forum.common.enums.ErrorCodeEn;
import com.hys.forum.common.enums.UserRoleEn;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.common.support.CheckUtil;
import com.hys.forum.pojo.Config;
import com.hys.forum.service.ConfigService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;


@Component
public class ConfigManager {

    @Resource
    private ConfigService configService;

    @IsLogin(role = UserRoleEn.ADMIN)
    public void add(ConfigAddRequest request) {
        Config config = ConfigTransfer.toConfig(request);
        config.setCreator(LoginUserContext.getUser().getId());

        configService.save(config);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void update(ConfigUpdateRequest request) {
        Config config = configService.get(request.getId());
        CheckUtil.isEmpty(config, ErrorCodeEn.CONFIG_NOT_EXIST);

        ConfigTransfer.update(config, request);
        configService.update(config);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void state(AdminBooleanRequest request) {
        Config config = configService.get(request.getId());
        CheckUtil.isEmpty(config, ErrorCodeEn.CONFIG_NOT_EXIST);

        config.setState(request.getIs() ? AuditStateEn.PASS : AuditStateEn.REJECT);
        configService.update(config);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public PageResponseModel<ConfigResponse> page(PageRequestModel<ConfigPageRequest> pageRequestModel) {
        ConfigPageRequest request = pageRequestModel.getFilter();

        PageResult<Config> pageResult = configService.page(PageUtil.buildPageRequest(pageRequestModel, ConfigTransfer.toConfig(request)));

        return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), ConfigTransfer.toConfigResponses(pageResult.getList()));
    }

    public List<ConfigResponse> queryAvailable(Set<String> types) {
        return ConfigTransfer.toConfigResponses(configService.queryAvailable(types));
    }
}
