package com.hys.forum.manager;

import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.vo.PostsVO;
import org.springframework.stereotype.Component;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.support.LoginUserContext;
import com.hys.forum.support.PageUtil;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.pojo.Posts;
import com.hys.forum.service.UserFeedService;

import javax.annotation.Resource;

@Component
public class PostsManager extends AbstractPostsManager {

    @Resource
    private UserFeedService userFeedService;

    @IsLogin
    public PageResponseModel<PostsVO> pagePostsFeed(PageRequestModel pageRequestModel) {
        PageResult<Posts> pageResult = userFeedService.pagePosts(PageUtil.buildPageRequest(pageRequestModel, LoginUserContext.getUser().getId()));

        return pagePostsVO(pageResult);
    }
}
