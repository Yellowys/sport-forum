package com.hys.forum.manager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.request.AdminBooleanRequest;
import com.hys.forum.pojo.request.article.*;
import com.hys.forum.pojo.response.article.ArticleInfoResponse;
import com.hys.forum.pojo.response.article.ArticleQueryTypesResponse;
import com.hys.forum.pojo.response.article.ArticleUserPageResponse;
import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.support.LoginUserContext;
import com.hys.forum.support.PageUtil;
import com.hys.forum.support.Pair;
import com.hys.forum.common.transfer.ArticleTransfer;
import com.hys.forum.common.enums.*;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.common.support.CheckUtil;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.pojo.*;
import com.hys.forum.pojo.value.PostsPageQueryValue;
import com.hys.forum.service.ArticleService;
import com.hys.forum.service.ArticleTypeService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


@Slf4j
@Component
public class ArticleManager extends AbstractPostsManager {

    @Resource
    private ArticleTypeService articleTypeService;

    @Resource
    private ArticleService articleService;

    private static final Long INIT_SORT = 1000L;

    @IsLogin
    @Transactional
    public Long saveArticle(ArticleSaveArticleRequest request) {
        // 创建
        if (ObjectUtils.isEmpty(request.getId())) {
            // 校验类型
            ArticleType articleType = checkArticleType(request.getTypeId());

            // 校验标签
            Set<Tag> selectTags = checkTags(request.getTagIds());

            Article article = ArticleTransfer.toArticle(request, articleType, selectTags, false, INIT_SORT);
            articleService.save(article);

            // 触发文章创建事件
            EventBus.emit(EventBus.Topic.ARTICLE_CREATE, article);

            return article.getId();
        }

        // 更新
        // 校验文章
        Article article = articleService.get(request.getId());
        CheckUtil.isEmpty(article, ErrorCodeEn.ARTICLE_NOT_EXIST);
        CheckUtil.isFalse(LoginUserContext.getUser().getId().equals(article.getAuthor().getId()), ErrorCodeEn.ARTICLE_NOT_EXIST);

        // 校验类型
        ArticleType articleType = checkArticleType(request.getTypeId());

        // 校验标签
        Set<Tag> selectTags = checkTags(request.getTagIds());

        // 删除旧标签关联关系
        tagService.deletePostsMapping(request.getId());

        Article oldArticle = article.copy();
        Article newArticle = ArticleTransfer.toArticle(request, articleType, selectTags, true, INIT_SORT);

        articleService.update(newArticle);

        // 触发文章更新事件
        EventBus.emit(EventBus.Topic.ARTICLE_UPDATE, Pair.build(oldArticle, newArticle));

        return request.getId();
    }

    public List<ArticleQueryTypesResponse> queryAllTypes() {
        List<ArticleType> articleTypes = articleTypeService.queryByState(AuditStateEn.PASS);

        return ArticleTransfer.toArticleQueryTypesResponses(articleTypes);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public List<ArticleQueryTypesResponse> queryAdminTypes() {
        List<ArticleType> articleTypes = articleTypeService.queryByState(null);

        return ArticleTransfer.toArticleQueryTypesResponses(articleTypes);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public PageResponseModel<ArticleQueryTypesResponse> typePage(PageRequestModel<ArticleAdminTypePageRequest> pageRequestModel) {
        ArticleAdminTypePageRequest typePageRequest = pageRequestModel.getFilter();
        ArticleType articleType = ArticleType.builder()
                .name(typePageRequest.getName())
                .description(typePageRequest.getDescription())
                .build();
        if (!ObjectUtils.isEmpty(typePageRequest.getAuditState())) {
            articleType.setAuditState(AuditStateEn.getEntity(typePageRequest.getAuditState()));
        }
        if (!ObjectUtils.isEmpty(typePageRequest.getScope())) {
            articleType.setScope(ArticleTypeScopeEn.getEntity(typePageRequest.getScope()));
        }
        PageResult<ArticleType> pageResult = articleTypeService.page(PageUtil.buildPageRequest(pageRequestModel, articleType));

        return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), ArticleTransfer.toArticleQueryTypesResponses(pageResult.getList()));
    }

    @IsLogin
    public List<ArticleQueryTypesResponse> queryEditArticleTypes() {
        List<ArticleTypeScopeEn> scopes = new ArrayList<>();
        scopes.add(ArticleTypeScopeEn.USER);

        User loginUser = LoginUserContext.getUser();
        if (!UserRoleEn.USER.equals(loginUser.getRole() )) {
            scopes.add(ArticleTypeScopeEn.OFFICIAL);
        }

        List<ArticleType> articleTypes = articleTypeService.queryByScopesAndState(scopes, AuditStateEn.PASS);

        return ArticleTransfer.toArticleQueryTypesResponses(articleTypes);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void addType(ArticleAddTypeRequest request) {
        CheckUtil.isNotEmpty(articleTypeService.query(ArticleType.builder()
                .name(request.getName())
                .build()), ErrorCodeEn.ARTICLE_TYPE_IS_EXIST);

        articleTypeService.save(ArticleTransfer.toArticleType(request));
    }

    public PageResponseModel<ArticleUserPageResponse> userPage(PageRequestModel<ArticleUserPageRequest> pageRequestModel) {
        PostsPageQueryValue pageQueryValue = PostsPageQueryValue.builder()
                .auditStates(Arrays.asList(AuditStateEn.PASS.getValue()))
                .build();

        ArticleUserPageRequest pageRequest = pageRequestModel.getFilter();
        if (!ObjectUtils.isEmpty(pageRequest.getTypeName())) {
            ArticleType articleType = articleTypeService.getByNameAndState(pageRequest.getTypeName(), AuditStateEn.PASS);
            CheckUtil.isEmpty(articleType, ErrorCodeEn.ARTICLE_TYPE_IS_EXIST);

            pageQueryValue.setTypeId(articleType.getId());
        }

        return pageQuery(pageRequestModel, pageQueryValue);
    }

    public PageResponseModel<ArticleUserPageResponse> authorPage(PageRequestModel<ArticleAuthorPageRequest> pageRequestModel) {
        ArticleAuthorPageRequest request = pageRequestModel.getFilter();

        List<String> auditStates = new ArrayList<>();
        auditStates.add(AuditStateEn.PASS.getValue());

        User user = LoginUserContext.getUser();
        if (!ObjectUtils.isEmpty(user) && user.getId().equals(request.getUserId())) {
            auditStates.add(AuditStateEn.WAIT.getValue());
            auditStates.add(AuditStateEn.REJECT.getValue());
        }

        PostsPageQueryValue pageQueryValue = PostsPageQueryValue.builder()
                .auditStates(auditStates)
                .authorId(request.getUserId())
                .build();

        return pageQuery(pageRequestModel, pageQueryValue);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public PageResponseModel<ArticleUserPageResponse> adminPage(PageRequestModel<ArticleAdminPageRequest> pageRequestModel) {
        ArticleAdminPageRequest request = pageRequestModel.getFilter();

        PostsPageQueryValue pageQueryValue = PostsPageQueryValue.builder()
                .category(PostsCategoryEn.ARTICLE.getValue())
                .authorId(request.getUserId())
                .typeId(request.getTypeId())
                .title(request.getTitle())
                .official(request.getOfficial())
                .marrow(request.getMarrow())
                .top(request.getTop())
                .build();
        if (!ObjectUtils.isEmpty(request.getAuditState()) && !ObjectUtils.isEmpty(AuditStateEn.getEntity(request.getAuditState()))) {
            List<String> auditStates = new ArrayList<>();
            auditStates.add(AuditStateEn.getEntity(request.getAuditState()).getValue());
            pageQueryValue.setAuditStates(auditStates);
        }

        return pageQuery(pageRequestModel, pageQueryValue);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void adminTop(AdminBooleanRequest booleanRequest) {
        BasePosts basePosts = postsService.get(booleanRequest.getId());
        CheckUtil.isEmpty(basePosts, ErrorCodeEn.ARTICLE_NOT_EXIST);

        basePosts.setSort(booleanRequest.getIs() ? 1L : INIT_SORT);
        basePosts.setTop(booleanRequest.getIs());
        postsService.update(basePosts);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void adminOfficial(AdminBooleanRequest booleanRequest) {
        BasePosts basePosts = postsService.get(booleanRequest.getId());
        CheckUtil.isEmpty(basePosts, ErrorCodeEn.ARTICLE_NOT_EXIST);

        basePosts.setOfficial(booleanRequest.getIs());
        postsService.update(basePosts);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void adminMarrow(AdminBooleanRequest booleanRequest) {
        BasePosts basePosts = postsService.get(booleanRequest.getId());
        CheckUtil.isEmpty(basePosts, ErrorCodeEn.ARTICLE_NOT_EXIST);

        basePosts.setMarrow(booleanRequest.getIs());
        postsService.update(basePosts);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void typeAuditState(AdminBooleanRequest booleanRequest) {
        ArticleType articleType = articleTypeService.get(booleanRequest.getId());
        CheckUtil.isEmpty(articleType, ErrorCodeEn.ARTICLE_TYPE_IS_EXIST);

        articleType.setAuditState(booleanRequest.getIs() ? AuditStateEn.PASS : AuditStateEn.REJECT);
        articleTypeService.update(articleType);
    }

    public ArticleInfoResponse info(Long id) {
        Article article = articleService.get(id);
        CheckUtil.isEmpty(article, ErrorCodeEn.ARTICLE_NOT_EXIST);

        if (!AuditStateEn.PASS.equals(article.getAuditState())) {
            User user = LoginUserContext.getUser();
            CheckUtil.isEmpty(user, ErrorCodeEn.ARTICLE_IN_AUDIT_PROCESS);
            CheckUtil.isFalse(user.getId().equals(article.getAuthor().getId()), ErrorCodeEn.ARTICLE_IN_AUDIT_PROCESS);
        }

        // 触发文章查看详情事件
        EventBus.emit(EventBus.Topic.POSTS_INFO, article);

        return ArticleTransfer.toArticleInfoResponse(article);
    }

    private PageResponseModel<ArticleUserPageResponse> pageQuery(PageRequestModel pageRequestModel, PostsPageQueryValue pageQueryValue) {
        pageQueryValue.setCategory(PostsCategoryEn.ARTICLE.getValue());

        PageResult<Article> pageResult = articleService.page(pageRequestModel.getPageNo(), pageRequestModel.getPageSize(), pageQueryValue);

        return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), ArticleTransfer.toArticleUserPageResponses(pageResult.getList()));
    }

    private ArticleType checkArticleType(Long typeId) {
        ArticleType articleType = articleTypeService.get(typeId);
        CheckUtil.isEmpty(articleType, ErrorCodeEn.ARTICLE_TYPE_IS_EXIST);
        CheckUtil.isFalse(AuditStateEn.PASS.equals(articleType.getAuditState()), ErrorCodeEn.ARTICLE_TYPE_IS_EXIST);

        return articleType;
    }

}
