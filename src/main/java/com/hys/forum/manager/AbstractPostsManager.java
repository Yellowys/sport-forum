package com.hys.forum.manager;

import com.hys.forum.pojo.request.AdminBooleanRequest;
import com.hys.forum.pojo.vo.PostsVO;
import com.hys.forum.service.CommentService;
import com.hys.forum.service.PostsService;
import com.hys.forum.service.TagService;
import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.support.LoginUserContext;
import com.hys.forum.common.transfer.PostsTransfer;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.common.enums.AuditStateEn;
import com.hys.forum.common.enums.ErrorCodeEn;
import com.hys.forum.common.enums.UserRoleEn;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.common.support.CheckUtil;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.pojo.BasePosts;
import com.hys.forum.pojo.Comment;
import com.hys.forum.pojo.Posts;
import com.hys.forum.pojo.Tag;


import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public abstract class AbstractPostsManager {

    @Resource
    TagService tagService;

    @Resource
    PostsService postsService;

    @Resource
    CommentService commentService;

    Set<Tag> checkTags(Set<Long> tagIds) {
        List<Tag> tags = tagService.queryByIds(tagIds);
        Set<Tag> selectTags = SafesUtil.ofList(tags).stream()
                .filter(tag -> AuditStateEn.PASS.equals(tag.getAuditState()))
                .collect(Collectors.toSet());
        CheckUtil.isEmpty(selectTags, ErrorCodeEn.TAG_NOT_EMPTY);

        return selectTags;
    }

    PageResponseModel<PostsVO> pagePostsVO(PageResult<Posts> pageResult) {
        Set<Long> solutionIds = SafesUtil.ofList(pageResult.getList()).stream()
                .filter(posts -> !ObjectUtils.isEmpty(posts.getSolutionId()) && posts.getSolutionId() != 0L)
                .map(Posts::getSolutionId).collect(Collectors.toSet());

        if (ObjectUtils.isEmpty(solutionIds)) {
            return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), PostsTransfer.toPostsVOs(pageResult.getList(), new ArrayList<>()));
        }

        List<Comment> comments = commentService.queryInIds(solutionIds);
        if (ObjectUtils.isEmpty(comments)) {
            return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), PostsTransfer.toPostsVOs(pageResult.getList(), new ArrayList<>()));
        }

        return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), PostsTransfer.toPostsVOs(pageResult.getList(), comments));
    }

    @IsLogin
    @Transactional
    public void delete(Long id) {
        BasePosts basePosts = postsService.get(id);
        CheckUtil.isEmpty(basePosts, ErrorCodeEn.POSTS_NOT_EXIST);
        CheckUtil.isFalse(LoginUserContext.getUser().getId().equals(basePosts.getAuthorId()), ErrorCodeEn.POSTS_NOT_EXIST);

        tagService.deletePostsMapping(id);
        commentService.deleteByPostsId(id);
        postsService.delete(id);
        EventBus.emit(EventBus.Topic.POSTS_DELETE, basePosts);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public void auditState(AdminBooleanRequest booleanRequest) {
        BasePosts basePosts = postsService.get(booleanRequest.getId());
        CheckUtil.isEmpty(basePosts, ErrorCodeEn.ARTICLE_NOT_EXIST);

        basePosts.setAuditState(booleanRequest.getIs() ? AuditStateEn.PASS : AuditStateEn.REJECT);
        postsService.update(basePosts);
    }
}
