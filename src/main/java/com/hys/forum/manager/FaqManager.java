package com.hys.forum.manager;

import com.hys.forum.pojo.response.faq.FaqHotsResponse;
import com.hys.forum.pojo.response.faq.FaqInfoResponse;
import com.hys.forum.pojo.response.faq.FaqUserPageResponse;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.request.faq.*;
import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.support.LoginUserContext;
import com.hys.forum.support.Pair;
import com.hys.forum.common.transfer.FaqTransfer;
import com.hys.forum.common.enums.AuditStateEn;
import com.hys.forum.common.enums.ErrorCodeEn;
import com.hys.forum.common.enums.PostsCategoryEn;
import com.hys.forum.common.enums.UserRoleEn;
import com.hys.forum.pojo.request.PageResult;
import com.hys.forum.common.support.CheckUtil;
import com.hys.forum.common.support.EventBus;
import com.hys.forum.common.support.SafesUtil;
import com.hys.forum.pojo.Comment;
import com.hys.forum.pojo.Faq;
import com.hys.forum.pojo.Tag;
import com.hys.forum.pojo.User;
import com.hys.forum.pojo.value.PostsPageQueryValue;
import com.hys.forum.service.CommentService;
import com.hys.forum.service.FaqService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class FaqManager extends AbstractPostsManager {

    @Resource
    private FaqService faqService;

    @Resource
    private CommentService commentService;

    @IsLogin
    @Transactional
    public Long saveFaq(FaqSaveFaqRequest request) {
        // 创建
        if (ObjectUtils.isEmpty(request.getId())) {
            // 校验标签
            Set<Tag> selectTags = checkTags(request.getTagIds());

            Faq faq = FaqTransfer.toFaq(request, selectTags, false);
            faqService.save(faq);

            // 触发创建事件
            EventBus.emit(EventBus.Topic.FAQ_CREATE, faq);

            return faq.getId();
        }

        // 更新
        // 校验问答
        Faq faq = faqService.get(request.getId());
        CheckUtil.isEmpty(faq, ErrorCodeEn.FAQ_NOT_EXIST);
        CheckUtil.isFalse(LoginUserContext.getUser().getId().equals(faq.getAuthor().getId()), ErrorCodeEn.FAQ_NOT_EXIST);

        // 校验标签
        Set<Tag> selectTags = checkTags(request.getTagIds());

        // 删除旧标签关联关系
        tagService.deletePostsMapping(request.getId());

        Faq oldFaq = faq.copy();
        Faq newFaq = FaqTransfer.toFaq(request, selectTags, true);

        faqService.update(newFaq);

        // 触发更新事件
        EventBus.emit(EventBus.Topic.FAQ_UPDATE, Pair.build(oldFaq, newFaq));

        return request.getId();
    }

    public PageResponseModel<FaqUserPageResponse> userPage(PageRequestModel<FaqUserPageRequest> pageRequestModel) {
        FaqUserPageRequest pageRequest = pageRequestModel.getFilter();

        PostsPageQueryValue pageQueryValue = PostsPageQueryValue.builder()
                .auditStates(Arrays.asList(AuditStateEn.PASS.getValue()))
                .build();

        if (!ObjectUtils.isEmpty(pageRequest.getSolution())) {
            if (pageRequest.getSolution() == 0) {
                pageQueryValue.setCommentId(1L);
            }
            if (pageRequest.getSolution() == 1) {
                pageQueryValue.setCommentId(2L);
            }
        }

        return pageQuery(pageRequestModel, pageQueryValue);
    }

    public PageResponseModel<FaqUserPageResponse> authorPage(PageRequestModel<FaqAuthorPageRequest> pageRequestModel) {
        FaqAuthorPageRequest request = pageRequestModel.getFilter();

        List<String> auditStates = new ArrayList<>();
        auditStates.add(AuditStateEn.PASS.getValue());

        User user = LoginUserContext.getUser();
        if (!ObjectUtils.isEmpty(user) && user.getId().equals(request.getUserId())) {
            auditStates.add(AuditStateEn.WAIT.getValue());
            auditStates.add(AuditStateEn.REJECT.getValue());
        }

        PostsPageQueryValue pageQueryValue = PostsPageQueryValue.builder()
                .auditStates(auditStates)
                .authorId(request.getUserId())
                .build();

        return pageQuery(pageRequestModel, pageQueryValue);
    }

    @IsLogin(role = UserRoleEn.ADMIN)
    public PageResponseModel<FaqUserPageResponse> adminPage(PageRequestModel<FaqAdminPageRequest> pageRequestModel) {
        FaqAdminPageRequest adminPageRequest = pageRequestModel.getFilter();
        PostsPageQueryValue pageQueryValue = PostsPageQueryValue.builder()
                .category(PostsCategoryEn.FAQ.getValue())
                .authorId(adminPageRequest.getUserId())
                .title(adminPageRequest.getTitle())
                .commentId(adminPageRequest.getCommentId())
                .build();

        if (!ObjectUtils.isEmpty(adminPageRequest.getAuditState()) && !ObjectUtils.isEmpty(AuditStateEn.getEntity(adminPageRequest.getAuditState()))) {
            List<String> auditStates = new ArrayList<>();
            auditStates.add(AuditStateEn.getEntity(adminPageRequest.getAuditState()).getValue());
            pageQueryValue.setAuditStates(auditStates);
        }

        return pageQuery(pageRequestModel, pageQueryValue);
    }

    public FaqInfoResponse info(Long id) {
        Faq faq = faqService.get(id);
        CheckUtil.isEmpty(faq, ErrorCodeEn.FAQ_NOT_EXIST);

        if (!AuditStateEn.PASS.equals(faq.getAuditState())) {
            User user = LoginUserContext.getUser();
            CheckUtil.isEmpty(user, ErrorCodeEn.FAQ_IN_AUDIT_PROCESS);
            CheckUtil.isFalse(user.getId().equals(faq.getAuthor().getId()), ErrorCodeEn.FAQ_IN_AUDIT_PROCESS);
        }

        // 触发查看详情事件
        EventBus.emit(EventBus.Topic.POSTS_INFO, faq);

        return FaqTransfer.toFaqInfoResponse(faq);
    }

    public List<FaqHotsResponse> hots(int size) {
        return FaqTransfer.FaqHotsResponses(faqService.hots(size));
    }

    @IsLogin
    public void solution(FaqSolutionRequest request) {
        Faq faq = faqService.get(request.getFaqId());
        CheckUtil.isEmpty(faq, ErrorCodeEn.FAQ_NOT_EXIST);
        CheckUtil.isFalse(LoginUserContext.getUser().getId().equals(faq.getAuthor().getId()), ErrorCodeEn.NO_PERMISSION);

        Comment comment = commentService.get(request.getCommentId());
        CheckUtil.isEmpty(comment, ErrorCodeEn.COMMENT_NOT_EXIST);

        faq.setSolutionId(request.getCommentId());
        faqService.updateEntity(faq);
    }

    private PageResponseModel<FaqUserPageResponse> pageQuery(PageRequestModel pageRequestModel, PostsPageQueryValue pageQueryValue) {
        pageQueryValue.setCategory(PostsCategoryEn.FAQ.getValue());
        PageResult<Faq> pageResult = faqService.page(pageRequestModel.getPageNo(), pageRequestModel.getPageSize(), pageQueryValue);

        Set<Long> solutionIds = SafesUtil.ofList(pageResult.getList()).stream()
                .filter(faq -> !ObjectUtils.isEmpty(faq.getSolutionId()) && faq.getSolutionId() != 0L)
                .map(Faq::getSolutionId).collect(Collectors.toSet());

        if (ObjectUtils.isEmpty(solutionIds)) {
            return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), FaqTransfer.toFaqUserPageResponses(pageResult.getList(), new ArrayList<>()));
        }

        List<Comment> comments = commentService.queryInIds(solutionIds);
        if (ObjectUtils.isEmpty(comments)) {
            return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), FaqTransfer.toFaqUserPageResponses(pageResult.getList(),  new ArrayList<>()));
        }

        return PageResponseModel.build(pageResult.getTotal(), pageResult.getSize(), FaqTransfer.toFaqUserPageResponses(pageResult.getList(), comments));
    }

}
