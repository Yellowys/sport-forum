package com.hys.forum.manager;

import org.springframework.stereotype.Component;
import com.hys.forum.pojo.request.file.FileUploadImgRequest;
import com.hys.forum.common.annotation.IsLogin;
import com.hys.forum.service.FileService;

import javax.annotation.Resource;


@Component
public class FileManager {

    @Resource
    private FileService fileService;

    @IsLogin
    public String uploadImg(FileUploadImgRequest request) {
        return fileService.uploadImg(request.getBase64(), request.getFileName());
    }
}
