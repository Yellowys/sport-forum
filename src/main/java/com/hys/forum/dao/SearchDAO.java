package com.hys.forum.dao;

import org.apache.ibatis.annotations.Param;
import com.hys.forum.pojo.dataobject.SearchDO;

import java.util.List;

/**
 * @author hys
 * @desc
 **/
public interface SearchDAO {

    void insert(SearchDO searchDO);

    void delete(@Param("type") String type, @Param("entityId") Long entityId);

    List<SearchDO> query(SearchDO searchDO);
}
