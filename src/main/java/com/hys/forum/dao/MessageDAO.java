package com.hys.forum.dao;

import org.apache.ibatis.annotations.Param;
import com.hys.forum.pojo.dataobject.MessageDO;

import java.util.List;

/**
 * @author hys
 * @desc
 **/
public interface MessageDAO {

    void insert(MessageDO messageDO);

    MessageDO get(Long id);

    List<MessageDO> query(MessageDO messageDO);

    void updateToRead(MessageDO messageDO);

    Long countUnRead(@Param("receiver") String receiver, @Param("read") String read);

    void deleteInTypesAndTitle(@Param("types") List<String> types, @Param("title") String title);
}
