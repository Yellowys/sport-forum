package com.hys.forum.dao;

import com.hys.forum.pojo.dataobject.OptLogDO;

import java.util.List;

/**
 * @author hys
 * @desc
 **/
public interface OptLogDAO {

    void insert(OptLogDO optLogDO);

    List<OptLogDO> query(OptLogDO optLogDO);
}
