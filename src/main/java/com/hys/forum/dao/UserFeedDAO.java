package com.hys.forum.dao;

import org.apache.ibatis.annotations.Param;
import com.hys.forum.pojo.dataobject.UserFeedDO;

import java.util.List;

/**
 * @author hys
 * @desc
 **/
public interface UserFeedDAO {

    void insert(UserFeedDO userFeedDO);

    List<UserFeedDO> query(@Param("userId") Long userId);

    void deleteByPostsId(@Param("postsId") Long postsId);
}
