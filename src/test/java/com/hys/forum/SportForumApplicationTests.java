package com.hys.forum;

import com.hys.forum.pojo.model.PageRequestModel;
import com.hys.forum.pojo.model.PageResponseModel;
import com.hys.forum.pojo.model.ResultModel;
import com.hys.forum.pojo.request.faq.FaqAdminPageRequest;
import com.hys.forum.pojo.response.faq.FaqUserPageResponse;
import com.hys.forum.service.api.FaqApiService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
@Slf4j
class SportForumApplicationTests {

    @Resource
    private FaqApiService faqApiService;
    @Test
    void contextLoads() {
        PageRequestModel<FaqAdminPageRequest> model = new PageRequestModel<>();
        ResultModel<PageResponseModel<FaqUserPageResponse>> page = faqApiService.adminPage(model);
        log.info(page.toString());
    }

}
